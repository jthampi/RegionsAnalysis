% Initialize parameters

% Script producing a movie of TIFF files that are actually RGB files. 

% To produce movies, make sure there are no thumbs in your directory
% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

[inout]=initialize_movie_generator

% Execute read_rawadata for first reading tiff files, etc, and creating data structure
inout.make_RGB_movie=1
inout.focus_on_single_z_tiff=0;
inout.select_z_stack=0;
inout.smooth_timecourse=0;
inout.smooth_in_z=0; % Smoothing option when focusing in a particular z, when doing a z-stack.  
inout.defaultnumberregions=5;

inout.max_fluor_to_plot=2500;

inout.open_several_files_in_directory=0;
inout.open_all_positions=1; % For opening all the positions in a directory.

inout.open_all_files_in_directory=0;

inout.defaultregiontype='Circle'
inout.make_basic_radial=0;
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;
inout.tiff_max_int=1;

inout.make_movie=0 % this is the important bit for deciding to make a movie or not
inout.make_movie_combinedchannels=1; 
inout.make_movie_juxtchannels=1;

inout.number_of_channels_to_compute_intensity=1; %put 2 if I want to make it active

inout.make_movie_juxtchannels=0;

inout.select_region=0;
inout.create_dirdata=0;
inout.analysis_regions=0;
inout.standard_format_filename=2
inout.threshold_backgroun_subtraction=0
inout.thresh_fluor_marker=0;  % from snap1 s1, t1.
inout.thresh_fluor_signal=0;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=0;

inout.numfiles_to_open=1;
%%

clear data
if inout.open_several_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


