function [data]=analysis_regions_basic_with_zs(inout,data,list_of_regions)

% This function makes a sandwitch (zsum) around an optimal z. At present,
% the optimal z is the one having highest mean intensity, or the one
% previously selected.

num_zs=size(data.temp_zstacks,3);
if num_zs<5
    msg=['Not enough z-planes to compute this mean intensity from a subset of 5 z-stacks.']; 
    error(msg);
end

for i=1:length(list_of_regions) 
   
regionnumber=list_of_regions(i);
ii=regionnumber;

%means_zs=zeros(num_zs);
for j=1:num_zs
selected_image_set=zeros(data.image_size(1),data.image_size(2),1);
selected_image_set(:,:,1)=data.temp_zstacks(:,:,j);
means_zs(j)=get_mean_fluor_in_region(data,ii,selected_image_set);
end % end of zs

if isfield(data,'selected_z')
optimal_z=data.selected_z;
else
 %[aux,int_peaks]=findpeaks(means_zs);
optimal_z=find(means_zs==max(means_zs)); %find maxima
   
end



if or(optimal_z<3,optimal_z>num_zs-3)
    msg=['Intensity maxima is close to the bottom or top of the z-stack. Not enough z-planes to compute this mean intensity from a subset of 5 z-stacks.']; 
    error(msg);
end

sum_means_zs=0;
for j=-2:2
    jj=optimal_z+j;
    sum_means_zs=sum_means_zs+means_zs(jj);
end

data.region{ii}.optimal_z=optimal_z;
data.region{ii}.zsum_mean_int_sandwich_optimal_z=sum_means_zs;

if inout.plot_fluor_surf_2channels==1
    plot_fluor_surf_2channels(inout,data,'region',regionnumber,'Surf_raw');
end

end % end of the region list tasks


close all

if inout.export_data_analysis==1
    save(fullfile(data.outdatapath, inout.data_structure_filename),'data');
    if inout.ok_boxes==1
        msgbox('Successfully exported data structure to the output folder.');
    end
end


