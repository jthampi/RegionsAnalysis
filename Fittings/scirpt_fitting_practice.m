Flin= @(par,x) par(1)+(x*par(2));
par0 = [max(ypoints) 1];
xpoints=[0 2 4 6 8]
ypoints=[0 4 8 12 16]
[rsquare_lin,par_lin]=fit_profile(Flin,par0,xpoints,ypoints)

Fexp= @(par,x) par(1)*exp(-(x/par(2)).^par(3))+par(4);
a = 140; b = 10; c = 2.9; d = 10;
par0=[a b c d]
ypoints=[150.0 148.249 143.574 136.485 127.482 117.093 105.849 94.2552 82.7625 71.7481 61.5031]
xpoints=[0 1 2 3 4 5 6 7 8 9 10]
[rsquare_exp,par_exp]=fit_profile(Fexp,par0,xpoints,ypoints);
