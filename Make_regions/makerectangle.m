function [rectanglePixels,markrectanglePixels]=makerectangle(x0,y0,x1,y1,x2,y2,imagesize)

imagesize_x = imagesize(2);
imagesize_y = imagesize(1);

[x y] = meshgrid(1:imagesize_y, 1:imagesize_x);


xs = [x0 x1 x1 x0 x0];
ys = [y0 y0 y2 y2 y0];

rectanglePixels = poly2mask(xs,ys,imagesize_y,imagesize_x);

markrectanglePixels=edge(rectanglePixels);
