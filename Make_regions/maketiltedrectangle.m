function [rectanglePixels,markrectanglePixels]=maketiltedrectangle(x0,y0,x1,y1,x2,y2,imagesize)

imagesize_x = imagesize(2);
imagesize_y = imagesize(1);

[x y] = meshgrid(1:imagesize_x, 1:imagesize_y);

I=zeros(imagesize_x,imagesize_y);
[x y]=bresenham(x0,y0,x1,y1);

for i=1:size(x,1)
    I(y(i),x(i))=1;
end
linePixels=I;


% insertShape function would be great to use, but a package is missing for that... 
%linePixels = insertShape(I, 'line', [x0 y0 x1 y1], 'LineWidth', 5);


B=(y1-y0)/(x1-x0);
A=y0-B*x0;

% perpendicular line to the line joining point 0 and 1
Bprim=-(x1-x0)/(y1-y0);
Aprim=y2-Bprim*x2;

x3=(A-Aprim)/(Bprim-B);
y3=A+B*x3;

vecx=(x2-x3);
vecy=(y2-y3);

x4=x0+vecx;
y4=y0+vecy;

x5=x1+vecx;
y5=y1+vecy;

xs = [x0 x1 x5 x4 x0];
ys = [y0 y1 y5 y4 y0];

rectanglePixels = poly2mask(xs,ys,imagesize_x,imagesize_y);

markrectanglePixels=edge(rectanglePixels);
