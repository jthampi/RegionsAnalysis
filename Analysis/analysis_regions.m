function [data]=analysis_regions(inout,data,list_of_regions)

%%% Parameters that can take 0 or 1 values
compute=1;
crit_conc=inout.crit_conc; %critical threshold fluorescense to define a threshold-based characteristic size

make_combinedmov_radial=0;
make_combinedmov=0;
%%%%%%%

if isfield(data,'region')
    switch data.region{1}.type 
        case 'Polygon'
            if strcmp(inout.polygontype,'opened') 
               data.fittings_path=fullfile(inout.datapath,'curve_fittings');
               create_dir(data.fittings_path)
            end
    end
end

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;

%if inout.analysis_polygons==1
%[data]=analysis_polygons(inout,data,regionnumber)
%end

if size(data.image_size)==2 % such that it is not implemented for rgb    
    if inout.background_subtraction==1
        selected_image_set=zeros(data.image_size(1),data.image_size(2),2);
        %selected_image_set(:,:,1)=data.back_sub_fluor_timecourse;
        selected_image_set(:,:,1)=data.first_image_backgr_sub;
    else  
        selected_image_set=zeros(data.image_size(1),data.image_size(2),2);
        selected_image_set(:,:,1)=data.first_image;
    end
end

%make_snap_all_regions(inout,data);
%make_snap_region(data,selected_image_set,regionnumber);
   % if ~(data.region{1}.type=='Line')
     % computing total intensity in the region
     if inout.number_of_channels_to_compute_intensity==1
        [data]=intensity_in_region(inout,data,regionnumber);
     elseif inout.number_of_channels_to_compute_intensity==2
        [data]=intensity_in_region_two_channels(inout,data,regionnumber);
     end
    %end
     % computing total intensity in the torus regions
   % checkifcircle=mean(data.region{ii}.type(1:6)=='Circle');    
   % if checkifcircle==1;
   %     [data]=intensity_circular_region_in_time(inout,data,regionnumber);
   % end
    
   % if checkifcircle==1;
   %    [data]=intensity_circular_region_in_time(inout,data,regionnumber);
  %  end

%

if make_combinedmov==1
    makemovie_combined_region(inout,data,selected_image_set,regionnumber);
end

if inout.compute_basic_properties_region==1
    data=get_basic_properties_region(inout,data,regionnumber);
end

if inout.compute_int_vs_r==1 %%%Work in ortho only for circle

    switch data.region{ii}.type 
        case 'Circle' 
            [data]=intensity_circular_region_in_time(inout,data,regionnumber);
        case 'Rectangle'
            [data]=intensity_rectangular_region_in_time(inout,data,regionnumber);
    end
    %[data]=compute_radial_production(data,regionnumber);
    % [data]=compute_tc_features(data,regionnumber);
    
    switch data.region{ii}.type 
        case 'Circle'
            ys=data.region{ii}.radial_concentrations;
        case 'Rectangle'
            ys=data.region{ii}.concentrations_in_x;
            selected_data_in_time=data.region{ii}.concentrations;
             if data.time_frames>1
                 plot_avInt_in_time(inout,data,regionnumber,selected_data_in_time,'avInt_vs_time')
             end         
    end

    if inout.make_basic_radial==1
        %plot_tc_features(data,regionnumber);
        %makemovie_prod_vs_r(data,regionnumber);
       % makemovie_Int_vs_r(data,regionnumber,data.region{ii}.radial_smooth_timecourse,'Int_smooth');
        if data.time_frames>1 
            makemovie_Int_vs_r(inout,data,regionnumber,ys,'Int');
        else
            plot_Int_vs_r(inout,data,regionnumber,ys,'Int');
        end
        %plot_radial_kymo(data,regionnumber);
    end

end

if inout.make_kymo_region==1
    [data]=make_kymo_region(inout,data,regionnumber);
end

%plot_fluorregion_surf(data,regionnumber,data.first_image,'Surf_raw');
%plot_fluorregion_surf(data,regionnumber,data.first_image_backgr_sub,'Surf');

if inout.plot_fluor_surf==1
    plot_fluor_surf(inout,data,'region',regionnumber,data.first_image,'Surf_raw');
    if inout.background_subtraction==1
        plot_fluor_surf(inout,data,'region',regionnumber,data.first_image_backgr_sub,'Surf');
    end
end

if inout.plot_fluor_surf_2channels==1
    plot_fluor_surf_2channels(inout,data,'region',regionnumber,'Surf_raw');
    if inout.background_subtraction==1
        plot_fluor_surf(inout,data,'region',regionnumber,data.first_image_backgr_sub,'Surf');
    end
end

if inout.make_fittings==1
    time_point=data.time_frames;
    strname='tryfit';
    %profile=data.region{ii}.radial_smooth_timecourse(:,:);
    %profile=data.region{ii}.radial_timecourse(:,:);
    profile=data.region{ii}.radial_concentrations;
    inout.plotfit=1;
    if inout.fit_hole==1
           % [data]=make_nonlinfit_profile_hole(data,time_point,profile,inout,crit_conc,regionnumber,strname);
            [data]=make_nonlinfit_profile_hole_former(data,time_point,profile,inout,crit_conc,regionnumber,strname);
    else
            [data]=make_nonlinfit_profile(data,time_point,profile,inout,crit_conc,regionnumber,strname);
            make_snap_of_region_size(inout,data,regionnumber);
    end
end

if make_combinedmov_radial==1
    selected_image_set=data.back_YFP_sub_fluor_timecourse;
    makemovie_combined_vs_r(data,selected_image_set,regionnumber,data.region{ii}.rs);
end

if inout.make_historegion==1
    [data]=plot_histo_region(inout,data,'region',regionnumber,data.first_image,'hist_raw');
    if inout.background_subtraction==1
        plot_histo_region(data,'region',regionnumber,data.first_image_backgr_sub,'hist_backsub')
    end 
end

if inout.find_orientation==1
    propaux=regionprops(data.region{regionnumber}.regionmask,'Orientation');
    [data.region{regionnumber}.mask_orientation]=propaux.Orientation;
end


switch data.region{ii}.type 
    case 'Polygon'
        if strcmp(inout.polygontype,'opened')   
           	data=get_basic_properties_curve(inout,data,regionnumber);
        end
end

% following has been commented 'cause it is not necessary for polygons.
% But it might be for 

%switch data.region{ii}.type 
%    case 'Polygon'
%        propaux=regionprops(data.region{regionnumber}.regionmask,'Centroid');
%        [data.region{regionnumber}.centroid]=propaux.Centroid;
%end
    
if inout.find_semiaxis==1
    propaux=regionprops(data.region{regionnumber}.regionmask,'MajorAxisLength','MinorAxisLength');
    if size(propaux,1)==1
        if inout.work_on_orthos==1
            [data.region{regionnumber}.major_axis_in_um]=propaux.MajorAxisLength*data.microns_per_pixel_orthos;
            [data.region{regionnumber}.minor_axis_in_um]=propaux.MinorAxisLength*data.microns_per_pixel_orthos;
        else
            [data.region{regionnumber}.major_axis_in_um]=propaux.MajorAxisLength*data.microns_per_pixel;
            [data.region{regionnumber}.minor_axis_in_um]=propaux.MinorAxisLength*data.microns_per_pixel;
        end
        [data.region{regionnumber}.aspect_ratio]=propaux.MajorAxisLength/propaux.MinorAxisLength;
    else
        disp(['Note: redo region ' regionnumber ', given it is understood as more than one region. First region taken by default to compute Minor and Major axis properties.'])
        if inout.work_on_orthos==1
            [data.region{regionnumber}.major_axis_in_um]=propaux(1).MajorAxisLength*data.microns_per_pixel_orthos;
            [data.region{regionnumber}.minor_axis_in_um]=propaux(1).MinorAxisLength*data.microns_per_pixel_orthos;
        else
            [data.region{regionnumber}.major_axis_in_um]=propaux(1).MajorAxisLength*data.microns_per_pixel;
            [data.region{regionnumber}.minor_axis_in_um]=propaux(1).MinorAxisLength*data.microns_per_pixel;
        end
        [data.region{regionnumber}.aspect_ratio]=propaux(1).MajorAxisLength/propaux(1).MinorAxisLength;            
    end
end

if inout.find_semiaxis==1
    propaux=regionprops(data.region{regionnumber}.regionmask,'MajorAxisLength','MinorAxisLength');
    if and(isfield(inout,'double_parabola'),size(data.regionlist,2)==inout.defaultnumberregions*2)  
        if and(inout.double_parabola==1,regionnumber>inout.defaultnumberregions)
            [DX,DY]=size(data.first_image_side);  
        else
            [DX,DY]=size(data.first_image);
        end
    else
        [DX,DY]=size(data.first_image);
    end
    if size(propaux,1)==1
        if and(DX==data.xys_image,DY==data.xys_image)
            [data.region{regionnumber}.major_axis_in_um]=propaux.MajorAxisLength*data.microns_per_pixel;
            [data.region{regionnumber}.minor_axis_in_um]=propaux.MinorAxisLength*data.microns_per_pixel;
        else
            [data.region{regionnumber}.major_axis_in_um]=propaux.MajorAxisLength*data.microns_per_pixel_orthos;
            [data.region{regionnumber}.minor_axis_in_um]=propaux.MinorAxisLength*data.microns_per_pixel_orthos;
        end
        [data.region{regionnumber}.aspect_ratio]=propaux.MajorAxisLength/propaux.MinorAxisLength;
    else
        disp(['Note: redo region ' regionnumber ', given it is understood as more than one region. Bigger region taken by default to compute Minor and Major axis properties.'])
        auxpropaux=[propaux.MajorAxisLength];
        MajorA=max(auxpropaux);
        auxpropaux=[propaux.MinorAxisLength];
        MinorA=max(auxpropaux);
        if and(DX==data.xys_image,DY==data.xys_image)
            [data.region{regionnumber}.major_axis_in_um]=MajorA*data.microns_per_pixel;
            [data.region{regionnumber}.minor_axis_in_um]=MinorA*data.microns_per_pixel;
        else
            [data.region{regionnumber}.major_axis_in_um]=MajorA*data.microns_per_pixel_orthos;
            [data.region{regionnumber}.minor_axis_in_um]=MinorA*data.microns_per_pixel_orthos;
        end        
        [data.region{regionnumber}.aspect_ratio]=MajorA/MinorA;            
    end
end

end % end of the region list tasks

if and(inout.select_background_regions==1,inout.plot_background_regions_stuff==1) 
    for i=1:length(data.backgroundregionlist) 
        regionnumber=data.backgroundregionlist(i);

        plot_fluor_surf(inout,data,'regionbackground',regionnumber,data.first_image,'surf_raw');
        plot_histo_region(data,'regionbackground',regionnumber,data.first_image,'hist_raw')
        
        plot_fluor_surf(inout,data,'regionbackground',regionnumber,data.first_image_backgr_sub,'surf_backsub');
        plot_histo_region(data,'regionbackground',regionnumber,data.first_image_backgr_sub,'hist_backsub')
    end    
end

if inout.number_of_channels_to_compute_intensity==2
    plot_all_regions_intensities(inout,data,data.regionlist);
end

close all
disp(['Analysis done for ' data.rootstring_first_image_name]);

if inout.export_data_analysis==1
    save(fullfile(data.outdatapath,inout.data_structure_filename),'data');
    if inout.ok_boxes==1
        msgbox('Successfully exported data structure to the output folder.');
    end
end


