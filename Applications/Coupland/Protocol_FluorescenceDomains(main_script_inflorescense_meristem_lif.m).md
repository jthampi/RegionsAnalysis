# QuickGuide for the use of `main_script_inflorescense_meristem_lif.m`
### Contact:
Pau Formosa-Jordan - pformosa@mpipz.mpg.de   
Pau Casanova-Ferrer - pcasanov@math.uc3m.es

## PRE-ANALYSIS: Optimal `.lif` structure
To ease and agile the analysis of the confocal images the script has two optimal file configurations:
1. Only one `.lif` file that contains all the images to analyse sequentially without non used images in between.
2. Several `.lif` files with the same number of images to analyse in all the files and located in the first positions of the `.lif` file.
In both cases, only the `.lif` files to analyse should be on the selected directory.

## ANALYSIS: Parameter definition
Right at the beginning of the script, we added the possibility of defining the location of the `.lif` file to reduce the need to manually select the directory:
```
%Comodities
here="/netscratch/dep_coupland/grp_formosa/pcasanova/Pau Martina/StacksEnric/onlyLD/10LD/Col-0/morpho";
cd(here);
```
Then the location of the `.lif` file should be introduced in the `here` variable. Then, when the pop-up window appears, you will only need to accept the default selection.

The Matlab script behaviour can be modified by the parameters in the structure `inout.`. Most of those parameters are set in the configuration file `Applications/Coupland/Initializations_Coupland/initialize_curve_script.m` but some of them can be modified in the script preamble. Here we will describe the most relevant ones:   

First, we will consider the parameters regarding the channel organization of the confocal images (located in lines 62-64)
```
%Channel selection:
inout.marker_channel_index_lif=1;       % marker channel number showing cell outlines (useful for Top automatic detection)
inout.signal_channel_index=2;           % channel to look at if lsm or lif files
```
From this section, `inout.marker_channel_index_lif` should point to the channel that contains the cell wall signal and `inout.signal_channel_index` should contain the channel number that contains the fluorescent marker to study.   

After this we have the parameters regarding the thresholding in lines 66-68:
```
% Thresholding:
inout.outsu_threshold_prefactor=1;      % prefactor for the otsu thresholding. If 1, the Otsu factor is taken. For dimmer signals take prefactors between 0 and 1.
inout.num_thresholds=2;                 %  number of thresholds to obtain via otsu thresholding. To avoid overlapping of nearby expression domains takes additional thresholds.
```
`inout.outsu_threshold_prefactor` defines a multiplicative prefactor for the Otsu thresholding. The default value is 1 but it can be reduced for images with a low signal when the mask is too small. 
`inout.num_thresholds` set the number of levels of intensity in which to divide the image. The default value, if this parameter is commented, is set to 1. But, to avoid the overlapping between the signal from the meristem and the primordia, it is better to divide the images into three regions and therefore set this parameter to 2.   

Following this, we have several parameters that we do not recommend modifying without contacting the developers (lines 70-79).

## ANALYSIS: Usage recommendations


## POST-ANALYSIS: Results location

