function make_snap_of_region_size(inout,data,regionnumber)
% This function is exporting the working image. Note will pick from 0 to
% 255 if inout.lsm_zmax==1.


[DX,DY]=size(data.selected_working_image);
if and(DX==data.xys_image,DY==data.xys_image)
    resolution=data.microns_per_pixel;
else
    resolution=data.microns_per_pixel_orthos;
end

font=18;

if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end

name=strcat(strng,'_Exp_domain.jpeg');

dirout=strcat('region_',num2str(regionnumber));
outfilename=fullfile(data.outdatapath,dirout,name);

origin=data.region{regionnumber}.origin;
x0=origin(1);y0=origin(2);
h=figure();

is=mat2gray(data.first_image);
circlemark=make_circlemark(data.region{regionnumber}.fitexp_rcharacteristic/resolution,x0,y0,is);
is=max(is,circlemark);

imshow(is);
hh=getframe(h);
hhh=frame2im(hh) ;
imwrite(hhh,outfilename);

%J = step(shapeInserter,I,PTS) 
%t = Tiff(outfilename,'w');
%t.write(hhh);
%t.close();


%print -dpdf -painters epsFig
end
