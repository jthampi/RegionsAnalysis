function [data]=add_regions_to_study(inout,data)
% add_region_to_study is a function that offers the possibility to add 
% new regions for studying to a previously existing data structure.

if(nargin < 1)
[data]=import_data;
end

[inout]=check_inout(inout);

%[f3,imm]=plot_combined_image(inout,data);
imm=data.image_with_regions;
if isfield('inout','label_type_in_snap')
    switch inout.label_type_in_snap
        case 'label'
            %[f3]=show_image_with_labels(inout,data,'InitialMagnification',inout.magnification)
            f3=figure();
            is=imshow(imm,'InitialMagnification',inout.magnification);           
        otherwise
            f3=figure();
            is=imshow(imm,'InitialMagnification',inout.magnification);
    end
else
    f3=figure();
    is=imshow(imm,'InitialMagnification',inout.magnification);
end

[data] = selecting_regions(inout,data,imm,f3);

end