function make_snap_all_regions(inout,data)
% Snap of data.image_with_regions, with the possibility to draw the regions
% outlines 

font=18;

switch inout.color_regions
    case 'blue'
        channelregion=3;
    otherwise 
        channelregion=2;
end

if ndims(image)==2
    channelregion=1;
end

sidepic=0;
if and(isfield(inout,'double_parabola'),size(data.regionlist,2)==inout.defaultnumberregions*2)  
    if inout.double_parabola==1  
        sidepic=1;        
    end
end

h=figure();
is=imshow(data.image_with_regions);
%is=imshow(data.image_with_regions,'Colormap',morgenstemning);



if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
    if sidepic==1        
        strng_side=[data.rootstring_first_image_name '_ortho_' inout.selected_channel_string];
    end
else
    strng=data.rootstring_first_image_name;
    if sidepic==1        
        strng_side=[data.rootstring_first_image_name '_ortho'];
    end
end

name=strcat(strng,'.jpeg');
outfilename=fullfile(inout.out_selected_regions_path,name);
if sidepic==1        
    name_side=strcat(strng_side,'.jpeg');
    outfilename_side=fullfile(inout.out_selected_regions_path,name_side);
end

namefig=strcat(strng,'.fig');
outfilenamefig=fullfile(inout.out_selected_regions_path,'fig_format',namefig);
if sidepic==1        
    namefig_side=strcat(strng_side,'.fig');
    outfilenamefig_side=fullfile(inout.out_selected_regions_path,'fig_format',namefig_side);
end

if inout.snap_regions_with_labels==1
    for ii=1:size(data.regionlist,2)
        pos=data.region{ii}.origin;

        if and(sidepic==1,ii==inout.defaultnumberregions+1)
            refresh(h)
            saveas(h,outfilename)
            savefig(outfilenamefig)  
            close(h);
            h_side=figure();
            istwo=imshow(data.image_with_regions_side);
        end
   % is = insertText(is,data.region{ii}.origin,data.region{ii}.label,'Textcolor','white','BoxOpacity',0)
%    data.image_with_regions(:,:,3)=max(data.image_with_regions(:,:,3),imag_with_regionlabels);
        switch inout.label_type_in_snap 
            case 'label'
                hnd1=text(pos(1)-10,pos(2),data.region{ii}.label);
            case 'class'
                hnd1=text(pos(1)-10,pos(2),num2str(data.region{ii}.class));
        end
        if inout.plot_ROIs_in_snap_all_regions==1
            set(hnd1,'FontSize',inout.fontsize,'Color',inout.color_labels)
            %BW8 = uint16(bwperim(data.region{ii}.regionmask,8));
            BW8 = bwperim(data.region{ii}.regionmask,8);
            BW8=imdilate(BW8, strel('disk',2));
            
            
            if and(sidepic==1,ii>inout.defaultnumberregions)
                mx=double(max(max(istwo.CData(:,:,channelregion))));
                if mx==0
                    mx=1;
                end
                istwo.CData(:,:,channelregion)=double(istwo.CData(:,:,channelregion))+mx*double(BW8);
            else
                mx=double(max(max(is.CData(:,:,channelregion))));
                if mx==0
                    mx=1;
                end
                is.CData(:,:,channelregion)=double(is.CData(:,:,channelregion))+mx*double(BW8); 
            end
        end
        %[B]=bwboundaries(data.region{ii}.regionmask);
        %plot(B{1}(:,1),B{1}(:,2))
    end
end
if sidepic==1 
    refresh(h_side)
    saveas(h_side,outfilename_side)
    savefig(outfilenamefig_side)
else
    refresh(h)
    saveas(h,outfilename)
    savefig(outfilenamefig)
end
    
%print -dpdf -painters epsFig
end
