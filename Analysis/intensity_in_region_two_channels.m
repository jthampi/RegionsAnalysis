function [data]=intensity_in_region_two_channels(inout,data,regionnumber)

% Intensity_in_region
% Note the pipeline is different for lsm files and for tiff files

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;

selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
if inout.background_subtraction==1
    if inout.islsm==1 
        selected_image_set(:,:,1,1)=data.first_image_backgr_sub;
    else
        displ('please update intensity_in_region')
        return
    end
else
    
    if inout.islsm==1 
        selected_image_set(:,:,1,1)=data.first_image;
    else
        % Note I might want to extend it also for signal quantification
            selected_image_set(:,:,1,:,1)=data.fluor_signal_timecourse;
            selected_image_set(:,:,1,:,2)=data.fluor_marker_timecourse;
    end
end

%microns_per_pixel=data.microns_per_pixel;


regionmask_Pixels=data.region{ii}.regionmask;
area_mask=sum(sum(regionmask_Pixels));

data.region{ii}.area_mask_in_pixels=area_mask;
if isfield(data,'resolution')
    [DX,DY]=size(selected_image_set(:,:,1,1));
    if and(DX==data.xys_image,DY==data.xys_image)
        microns_square_per_pixel=data.microns_square_per_pixel;
        microns_cube_per_pixel=data.microns_square_per_pixel*data.microns_per_pixel_orthos;
    else
        microns_square_per_pixel=data.microns_square_per_pixel_orthos;
        microns_cube_per_pixel=data.microns_square_per_pixel_orthos*data.microns_per_pixel;
    end
    area_mask_in_microns_sq=area_mask*microns_square_per_pixel;
    data.region{ii}.area_mask_in_microns_sq=area_mask_in_microns_sq;
end

if data.time_frames>1
    hwait=waitbar(0,'% of progress for time course profile computation'); % generates a waitbar
end

for k=1:data.time_frames
    for channum=1:2
        data.region{ii}.intensity(k,channum)=sum(sum(regionmask_Pixels.*selected_image_set(:,:,1,k,channum)));
        % zoom correction if the field exists 
        if isfield(data,'resolution')
            data.region{ii}.intensity(k,channum)=microns_cube_per_pixel*data.region{ii}.intensity(k,channum);
            data.region{ii}.mean_intensity(k,channum)=data.region{ii}.intensity(k,channum)/area_mask_in_microns_sq;
        else
            data.region{ii}.mean_intensity(k,channum)=data.region{ii}.intensity(k,channum)/area_mask;
        end
    end
    data.region{ii}.intensity_ratio(k)=data.region{ii}.intensity(k,1)/data.region{ii}.intensity(k,2);
    data.region{ii}.mean_intensity_ratio(k)=data.region{ii}.mean_intensity(k,1)/data.region{ii}.mean_intensity(k,2);

end

if data.time_frames>1
    close(hwait);
end

end


