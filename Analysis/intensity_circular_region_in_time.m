function [data]=intensity_circular_region_in_time(inout,data,regionnumber)

% Intensity_circular_region_in_time performins full or partial angle integration
% of the intensity

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;

if inout.background_subtraction==1
    %selected_image_set=data.back_YFP_sub_fluor_timecourse;     
    selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
    selected_image_set(:,:,1,1)=data.first_image_backgr_sub;   
else
    selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
    selected_image_set(:,:,1,data.time_frames)=data.first_image;
end

maximumradius=data.region{ii}.radius;
%radiusstep=data.radiusstep;
%radiusstep=12;

%data.region{ii}.radiusstep=inout.radiusstep;

data.region{ii}.radiusstep=data.radiusstep;

radiusstep=data.region{ii}.radiusstep;
[DX,DY]=size(data.first_image);
if and(DX==data.xys_image,DY==data.xys_image)
    microns_per_pixel_surface=data.microns_per_pixel;
    microns_per_pixel_projection=data.microns_per_pixel_orthos;
else
    microns_per_pixel_surface=data.microns_per_pixel_orthos;
    microns_per_pixel_projection=data.microns_per_pixel;
end
origin=data.region{regionnumber}.origin;
x0=origin(1);y0=origin(2);
frac_angularregion=data.region{regionnumber}.frac_angularregion;
regionmask_Pixels=data.region{regionnumber}.regionmask;
iniradius=radiusstep;
radii=double((iniradius:radiusstep:maximumradius));
rs=double(radii)*microns_per_pixel_surface-double(radiusstep)*microns_per_pixel_surface/2.0;
rs(1)=0; % NOTE: BE AWARE OF THIS DISCRETIZATION CHOICE
data.region{ii}.rs=rs; % NOTE: this is defined for representation purposes

%data.region{ii} = setfield(data.region{ii}, 'annuli', cell(data.time_frames,1));

ind=0; hwait=waitbar(0,'% of progress for time course profile computation'); % generates a waitbar
for k=1:data.time_frames
%for k=1:2
    waitbar(1.0*ind/data.time_frames); ind=ind+1; 
    image=selected_image_set(:,:,1,k);
    image_tomeasure=regionmask_Pixels.*image;
 
    [intensity_radii, annuli]=angular_integration2(image_tomeasure,x0,y0,maximumradius,radiusstep,microns_per_pixel_surface,frac_angularregion);
    data.region{ii}.radial_concentrations(:,k)=intensity_radii*microns_per_pixel_projection;
    
    % note that we don't need correction of the fluorescense here due to
    % zoom. See code in angular_integration2; we are computing a
    % concentration of intensity:
    %[I]= (sum pix intensity *resolution)/(number of pixels in the torus*
    %resolution) -> the resolution is cancelled out!
    
    %%%Casanova 2021-05-26
    %As we are measuring the intensity of a projection is a volumetric intensity 
    %so we need to multiply by the zresolution.
        
    % PFJ 2015-08-13 I commented it 'cause I don't remember what it is for
%     data.region{ii}.annuli{k} = annuli;
%    for i=1:size(annuli,3) % go through the different circular-donut masks 
%        temp = reshape(annuli(:,:,i),512*512,1); % put such mask multiplied by the fluorescense in a 1-d vector
%        data.region{ii}.annuli{k,i} = temp(temp~=0);
%    end
end

close(hwait);





