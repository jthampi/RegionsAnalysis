function [dirdata]=get_dirdata_comulative_numregions_indices(inout,dirdata)

% Indexing the ROIs of different datasets when put altogether. This might be important
% when managing different datasets at a time, what happens at creating the dirdata.features field in the extract_features function.

num_datasets=size(dirdata.dataset,2);

aux_dataset_numregions=0;
datasets_numregions_list=[];
cumulative_numregions_list=[];
cumulative_numregion_per_dataset=[];

for ii=1:num_datasets
    cumulative_numregion_per_dataset=[cumulative_numregion_per_dataset aux_dataset_numregions];
    dataset_numregions=size(dirdata.dataset{ii}.regionlist,2);
    datasets_numregions_list=[datasets_numregions_list dataset_numregions];
    ordrvect=zeros(1,dataset_numregions)+aux_dataset_numregions;
    aux_dataset_numregions=aux_dataset_numregions+dataset_numregions;
    cumulative_numregions_list=[cumulative_numregions_list ordrvect]; 
end

dirdata.datasets_numregions_list=datasets_numregions_list;
dirdata.cumulative_numregions_list=cumulative_numregions_list;
dirdata.cumulative_numregion_per_dataset=cumulative_numregion_per_dataset;

end