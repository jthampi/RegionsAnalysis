function [data]=make_nonlinfit_profile(data,time_point,profile,inout,crit_conc,regionnumber,strname);
% This function fits the data into a generalized exponential profile and to
% a hill function, and creates the corresponding plots. 

if isfield(inout,'selected_channel_string')
    data.selected_channel_string=inout.selected_channel_string;
end

plotwinfit=inout.plotwinfit;
plotfits=inout.plotfits;

font=20;
conc_initial_point=100; % initial point for root finding

frac_angularregion=data.region{regionnumber}.frac_angularregion;
maximumradius=data.region{regionnumber}.radius;
radiusstep=data.region{regionnumber}.radiusstep;


%ii=find(data.regionlist==regionnumber);
ii=regionnumber;
dirout=strcat('region_',num2str(regionnumber));
%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.twochannels_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep));
name=strcat('Fit',strname,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');

outfilename=fullfile(data.outdatapath,dirout,name);

xpoints=data.region{ii}.rs;
jj=time_point; %% this sets the time point
ypoints=transpose(profile(:,jj));

Fexp= @(par,x) par(1)*exp(-(x/par(2)).^par(3))+par(4);
Fhill= @(par,x) par(1)./(1+(x/par(2)).^par(3))+par(4);
par0 = [max(ypoints) 10.0 1.2 min(ypoints)];
[rsquare_exp,par_exp]=fit_profile(Fexp,par0,xpoints,ypoints);
%'Exp fit done'
[rsquare_hill,par_hill]=fit_profile(Fhill,par0,xpoints,ypoints);
%'Hill fit done'

%Flin= @(par,x) par(1)-(x*par(2));
%par0 = [max(ypoints) 1];
%[rsquare_lin,par_lin]=fit_profile(Flin,par0,xpoints,ypoints);

may=max(ypoints);
miy=min(ypoints);

if inout.r_crit_thresh==1 
    zerofunc=@(x) par_exp(1)*exp(-(x/par_exp(2)).^par_exp(3))+par_exp(4)-crit_conc;
    if and(and(crit_conc>miy,crit_conc<may),rsquare_exp>0.5)
        r_crit_exp=fzero(zerofunc,conc_initial_point);
        %print('yes')
    else
        r_crit_exp=NaN;
        %print('no')
    end

    zerofunc=@(x) par_hill(1)./(1+(x/par_hill(2)).^par_hill(3))+par_hill(4)-crit_conc;

    if and(and(crit_conc>miy,crit_conc<may),rsquare_hill>0.5)
        r_crit_hill=fzero(zerofunc,conc_initial_point);
        %print('yes')
    else
        r_crit_hill=NaN;
        %print('no')
    end
end

if max(rsquare_exp,rsquare_hill)==rsquare_exp;
    bestfit=0;
    fitto=' Exp ';
    par_win=par_exp;
    winfunc=@(x) par_exp(1)*exp(-(x/par_exp(2)).^par_exp(3))+par_exp(4);
    rsquare=rsquare_exp;

else
    bestfit=0;
    fitto=' Hill ';
    par_win=par_hill;
    winfunc=@(x) par_hill(1)./(1+(x/par_hill(2)).^par_hill(3))+par_hill(4);
    rsquare=rsquare_hill;  
end

% Defining the two fitted functions
Fexpfit= @(x) par_exp(1)*exp(-(x/par_exp(2)).^par_exp(3))+par_exp(4);
Fhillfit= @(x) par_hill(1)./(1+(x/par_hill(2)).^par_hill(3))+par_hill(4);

data.region{regionnumber}.fitexp_pars=[par_exp];
data.region{regionnumber}.fitexp_rcharacteristic=par_exp(2)*(log(2).^(1/par_exp(3)));
data.region{regionnumber}.fitexp_rsquare=[rsquare_exp];
data.region{regionnumber}.fithill_pars=[par_hill];
data.region{regionnumber}.fithill_rcharacteristic=par_hill(2);
data.region{regionnumber}.fithill_rsquare=[rsquare_hill];
data.region{regionnumber}.bestfit=fitto;

if inout.r_crit_thresh==1 
    data.region{regionnumber}.fitexp_rcritthresh=[r_crit_exp];
    data.region{regionnumber}.fithill_rcritthresh=[r_crit_hill];
end

if data.microns_per_pixel==1
    xlab='r [A.U.]';
else
    xlab='r [\mum]';
end
ylab='Fluorescense [A.U.]';

if plotwinfit==1
     
    h=figure();
    plot(xpoints,winfunc(xpoints),'-r','linewidth',2);   % plot fit 
    hold on
    plot(xpoints,ypoints,'bo');   % plot concentration levels 
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    legend(strcat(fitto,' Fit'),'Data')
    save2pdf(outfilename,h);
    close(h)
    %saveplot('fit',params,h,output);
end


if plotfits==1
    
    h=figure();
    plot(xpoints,Fexpfit(xpoints),'-r','linewidth',2);   % plot fit 
    hold on
    plot(xpoints,ypoints,'bo');   % plot concentration levels 
    xline(par_exp(2)*(log(2).^(1/par_exp(3))),'--k','linewidth',2);
    yline(par_exp(4),'--b','linewidth',2);
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    legend(' Exp Fit','Data',strcat(' ',num2str(par_exp(2)*(log(2).^(1/par_exp(3)))),'\mum'),strcat(' ',num2str(round(par_exp(4),1)),' A.U.'))
%     xlim([0,50])
%     ylim([0,1650])
    name=strcat('ExpFit',strname,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');

    outfilename=fullfile(data.outdatapath,dirout,name);

    save2pdf(outfilename,h);
    
    close(h)
    
    
    h=figure();
    plot(xpoints,Fhillfit(xpoints),'-r','linewidth',2);   % plot fit 
    hold on
    plot(xpoints,ypoints,'bo');   % plot concentration levels 
    xline(par_hill(2),'--k','linewidth',2);
    yline(par_hill(4),'--b','linewidth',2);
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    legend(' Hill Fit','Data',strcat(' ',num2str(par_hill(2)),'\mum'),strcat(' ',num2str(round(par_hill(4),1)),' A.U.'))
%     xlim([0,50])
%     ylim([0,1650])
    
    name=strcat('HillFit',strname,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');

    outfilename=fullfile(data.outdatapath,dirout,name);

    save2pdf(outfilename,h);
    close(h)
    
    %saveplot('fit',params,h,output);
end

plotfits_together=0;

if plotfits_together==1    
    h=figure();
    plot(xpoints,Fexpfit(xpoints),'-r','linewidth',2);   % plot fit
    hold on
    plot(xpoints,Fhillfit(xpoints),'--','MarkerEdgeColor',[0.5 0.5 0.5],'linewidth',3);   % plot fit 
    hold on
    plot(xpoints,ypoints,'bo');   % plot concentration levels 
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    legend(' Exp Fit','Hill Fit','Data')
    xlim([0,50])
    ylim([0,1650])
    name=strcat('ExpandHillFit',strname,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');

    outfilename=fullfile(data.outdatapath,dirout,name);

    save2pdf(outfilename,h);
    
    close(h)
    
end


end
