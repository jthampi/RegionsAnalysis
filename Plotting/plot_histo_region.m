function [data]=plot_histo_region(inout,data,region_string,regionnumber,fluor,namestr)

temp_region=getfield(data,region_string);

ii=regionnumber;

regionmask=temp_region{ii}.regionmask;

h=figure(10);

preimage_in_region=fluor;
indices_mask=find(regionmask==1);
image_in_region=preimage_in_region(indices_mask);
pixelmax=max(max(fluor));
nbins=40;
hh=histogram(image_in_region,nbins,'BinLimits',[0,pixelmax]);
profile=hh.Values;
edgesfrombins=hh.BinEdges;
%[aux,int_peaks]=findpeaks(profile);

h2 = gcf;
hold on
if isfield(inout,'histomaxima')
    if inout.histomaxima==1
        figure()
        hh2=histogram(image_in_region,nbins);[auxb,int_peaksb]=findpeaks(image_in_region);

        [smooth_histofunc,xi]=ksdensity(image_in_region) ;
        %figure()
        %plot(xi,smooth_histofunc)

        [aux,int_peaks]=findpeaks(smooth_histofunc);
       % histomaxima=hh2.BinLimits(1)+(hh2.BinLimits(2)-hh2.BinLimits(1))*int_peaks/size(smooth_histofunc,2) %-hh.BinWidth/2;
        
        figure(10)
        %hold on
        if size(int_peaks,2)>inout.allowed_peaks
            msg=['Revise assign a longer number of allowed peaks']; 
            error(msg);
            return
        end
        histomaxima=zeros(1,inout.allowed_peaks);
        for yi=1:size(int_peaks,2)
            histomaxima(1,yi)=xi(int_peaks(yi));
            plot(histomaxima(1,yi)*ones([30,1]),1:100:3000);
            %plot(*ones([30,1]),1:100:3000);
        end
        data.region{ii}.histomaxima=histomaxima;
    end
end

xlabel('Pixel Intensity'); ylabel('Counts');
data.region{ii}.hist=h2; 
regionnumber=ii;
name=strcat(namestr,'_',region_string,num2str(regionnumber),'_',data.selected_channel_string,'.pdf');

dirout=strcat(region_string,'_',num2str(regionnumber));

outfilename=fullfile(data.outdatapath,dirout,name);
%savefig(outfilename);
save2pdf(outfilename,h);



close(h)


