function [inout]=check_inout(inout)

% check_inout function performs some controls on the input variables

if and(inout.standard_format_filename==0,inout.timecourse==1);
disp('Sorry, no time course supported for files not written in standard format');
inout.return=1;
    return ;
else
    inout.return=0;    
end

if inout.overwriting_mode==0
    inout.get_polygon_from_previous_data=0;
end


if and(inout.background_subtraction==1,inout.select_background_regions==0)
    inout.select_background_regions=1;
    disp('Note inout parameter inconsistency, has been set to inout.select_background_regions=1');
end

if inout.make_fittings==0
    inout.post_analysis=0;
end

% in order to get the centroid of the central domain, first we must find
% the central domain
if inout.get_center_from_cental_domain==1;
    inout.find_central_domain=1;
end

if or(inout.open_all_files_in_directory==1,inout.open_all_positions==1)
    inout.open_several_files_in_directory=1;
else
    inout.open_several_files_in_directory=0;
end

%if isfield(inout,'init_file_to_open')
%    if('init_file_to_open'>1)
%        inout.create_dirdata=0;
%    end
%end

if isfield(inout,'defaultregiontype')
    poly_string='Polygon';
    num_chars_to_compare=min(size(inout.defaultregiontype,2),size(poly_string));
    if  not(mean(inout.defaultregiontype(1:num_chars_to_compare)==poly_string(1:num_chars_to_compare))==1)
        inout.center_click=1;
        inout.ispolygon=0;
    else
        inout.center_click=0;
        inout.ispolygon=1;
    end
end

if isfield(inout,'get_polygon_from_previous_data')
    if inout.get_polygon_from_previous_data==1
        inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
        inout.click_selecting_regions=0;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
    end
end

if isfield(inout,'get_centroids_from_found_domains')
    if inout.get_centroids_from_found_domains==1
        inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
        inout.click_selecting_regions=0;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
    end
end


%inout.signal_channel_index=inout.lsm_signal_channel_index;         %   channel to look at if lsm files
%inout.signal_channel_index=inout.lsm_signal_channel_index;         %   channel to look at if lsm files
%inout.zsum=inout.lsm_zsum;                         %   if we want to do a zsum 
%inout.zmax=inout.lsm_zmax;
%inout.marker_channel_index=inout.lsm_marker_channel_index;

switch inout.analysis_type
    case 'Multi-z'
        inout.analysis_in_zs=1;
        
end

if isfield(inout,'get_polygon_from_previous_data')
    if inout.get_polygon_from_previous_data==1
        inout.number_of_regions_setting='by_number';
    end
end

switch inout.number_of_regions_setting
    case 'on_the_fly'
        inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
end

%if isfield(inout,'signal_channel_index_lif')
%    inout.signal_channel_index=inout.signal_channel_index_lif;
%end

%if isfield(inout,'marker_channel_index_lif')
%    inout.signal_channel_index=inout.marker_channel_index_lif;
%end



end