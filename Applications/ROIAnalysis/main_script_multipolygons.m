%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_multipolygons Matlab script allows to draw different regions of
% interest (ROI) in lsm, lif and tiff files, and extracts its characteristics. This script has
% been optimised for drawing polygons as ROIs.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:

% 1) inout structure : provides parameters and strings in relation to input
% and output

% 2) data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.

% 3) dirdata structure: structure compiling the different data structures 
% within a folder and some analysis of key features of the different data
% sets.

% If loading the dirdada structure, you can access the regions analysis 
% by typing dirdata.dataset{i}.region{j}, being i and j two indices.

% See README.md document for further explanations about the code.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_multipolygons_twochannels function to load the default parameters for the analysis. 
[inout]=initialize_basic_multipolygons;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
% NOTE: do not move these parameters above the initialization line.

inout.get_polygon_from_previous_data=0; % set 1 to get polygons from previous data, otherwise set it to 0. Note that if setting this to 1, if you did not analyse all images from the lif project, you need to specify parameter inout.numfiles_to_open

inout.make_zoom=0; % set to 1 if zoom needed for drawing ROI, and 0 otherwise. When set to 1, place cursor in the region where zoom is needed, and press click.  

% WARNING: for using the zoom function, there is a missing external function in the
% current code that I can not provide due to a copyright limitation (the makergb function, which can be extracted from Schnitzcells software).

inout.magnification=300; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.
inout.zoom_magnification=1200;
inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not having to set the number of regions to mark in advance. When being 'on_the_fly' mode, press space bar to start selecting the next ROI, or press key 'q' to finish selecting ROIs in the current image. Otherwise, set 'by_number'.
inout.extract_features=1;
inout.find_orientation=1;
inout.number_of_channels_to_compute_intensity=0;

inout.compute_basic_properties_region=1;
inout.overrideformat=1; % to impose the format from the inout provided structure set it to 1
inout.format='lsmczi'; % format of the image; 'lsmczi', 'tiffrgb', 'tif', 'lif'
inout.zmax=1;        % parameter for lsm or czi z-stacks. set it to 1 to make z-sum, otherwise set it to 0.
inout.color_regions='blue';
inout.standard_format_filename=6; % for a non-tiff format, set it to a number higher than 3
inout.save_while_selecting_regions=1; % setting this parameter to 1 will save the regions while selecting them - so if there is a crash while selecting, one can still recover them, and keeping adding regions afterwards. This might slow down the code 


inout.draw_freehand_polygon=1; % enables drawing of the polyigon in a
%freehand style. Otherwise set it to 0, and multiple clicks will be needed
%to plot the polygon. Option available just without zoom

% for tiffs, uncomment the following inout lines
%inout.format='tif';
%inout.standard_format_filename=1 %  for tiff format, set it to 1-3 (see RegionsAnalysis.m)
%inout.scope='spiscancit';
%inout.number_tiff_channels=1;

%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout);
else
    [data]=RegionsAnalysis(inout);
end
inout.save_backup_regions_step=20;

%%

% To add extra regions, execute the following 
% to use this function, note that inout.save_while_selecting_regions
% parameter should have been set to 1 previously

%In case the data directory has been selected (i.e., inout.datapath has
%been determined) 
add_regions_and_resume_analysis(inout.datapath)

%% 
% In case we want to select again the data folder
add_regions_and_resume_analysis

%%

%%
% For the rgbtiff files, resolution needs to be set by hand, given that the
% Keyence metadata is not correctly extracted 

%inout.datapath='blablabla2'
extras.resolution=[1 1] ;
save(fullfile(inout.datapath,'extras'),'extras');

