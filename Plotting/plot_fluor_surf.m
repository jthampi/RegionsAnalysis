function plot_fluor_surf(inout,data,region_string,regionnumber,fluor,namestr)

%ii=find(data.regionlist==regionnumber);
temp_region=getfield(data,region_string);

ii=regionnumber;

regionmask=temp_region{ii}.regionmask;
[row, col]=find(regionmask);


dirout=strcat(region_string,'_',num2str(regionnumber));

font=18;
name=strcat(namestr,'_',data.selected_channel_string,'.pdf');

outfilename=fullfile(data.outdatapath,dirout,name);

if ~isfield(data,'resolution')
%if data.microns_per_pixel==1
    xlab='x [A.U.]';
    ylab='y [A.U.]';
else
    xlab='x [\mum]';
    ylab='y [\mum]';

end
% for the moment I let it written in pixels...
xlab='x [A.U.]';
ylab='y [A.U.]';


ind=0; hwait=waitbar(0,'% of progress for plot generator'); % generates a waitbar

for k=1:data.time_frames
    waitbar(1.0*ind/(data.time_frames-1)); ind=ind+1; 
    h=figure();
    
    regionmask=double(regionmask);
    regionmask(regionmask==0)=NaN;

    
    flourinmask=double(regionmask).*double(fluor); 

    
    boundregionmask=regionmask(min(row):max(row),min(col):max(col));
    boundflourinmask=flourinmask(min(row):max(row),min(col):max(col));


    reduce=1;

    regiontoplot=double(boundflourinmask(1:reduce:end,1:reduce:end));

    surfc(regiontoplot)
    shading interp

    
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense [A.U.]','fontsize',font)
    zlim([0 inout.max_fluor_to_plot])
    
    set(gca,'fontSize',font);
    hh=gcf;
    savefig(outfilename(1:length(outfilename)-3));
    %toc
    %save2pdf(outfilename,hh);
    close(h)
end

close(hwait);




