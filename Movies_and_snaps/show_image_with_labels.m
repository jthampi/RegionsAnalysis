function [outfig]=show_image_with_labels(inout,data)
% This function is just to show the different labels of the marked ROIs.
[outfig]=figure();
%imshow(data.image_with_regions,'InitialMagnification',inout.magnification)
imshow(data.image_with_regions)
num_cells=length(data.regionlist)
for i=1:num_cells
pos=data.region{i}.origin;
hnd1=text(pos(1)-10,pos(2),data.region{i}.label);
set(hnd1,'FontSize',16,'Color','white')
end

disp(['Showing ' data.datapath])
end
