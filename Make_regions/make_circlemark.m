function circlemark=make_circlemark(radius,x0,y0,image)

% this function gives back a circle mask of a certain width that will be
% embedded in an image (2D matrix) afterwards by doing
% max(image,circlemark)

width=2.0;
imagesize=size(image);


circlePixels=makecircle(radius,x0,y0,imagesize);
circlePixelsbis=makecircle(radius-width,x0,y0,imagesize);
circlemark=(circlePixels-circlePixelsbis);


