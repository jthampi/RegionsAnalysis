ShootMeristemsAnalysis code: Analysis of the fluorescence expression domains in the Arabidopsis shoot meristems
-----------------------------------------------------------------------------------------------------------------------------------------

This Code has been used for the following publication XXX (2019):

Title: XYZXYZ

Authors: Pau Formosa-Jordan (1,2), Benoit Landrein (1,2).

1. Sainsbury Laboratory, University of Cambridge, Bateman Street, Cambridge CB2 1LR, UK.
2. Max Planck Institute for Plant Breeding Research, Cologne, Germany.
3. Laboratoire Reproduction et Développement des Plantes, Univ Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRA, Lyon, France.

Contact: Pau Formosa-Jordan - pformosa@mpipz.mpg.de


INSTALLATION NOTES
---------------------------

This code has been run using MATLAB 20018b with Windows 7 Professional and in Mac (OS X Mojave). To use the code, just open and execute any of the main_scripts in the present subfolder.  

For further details about the installation, see the main Readme file of the RegionsAnalysis repository.   


SUMMARY
------------

(Need to be updated, in process)  

 script is used for computing the
characteristic length of expression domains at the shoot apical meristem
for different fluorescent reporters. The intensity of such fluorescent domains is
computed as well. Note this script is very similar to the RegionsAnalysis script main_script_meristem_Landrein_etal_2018

The code can automatically find the central flourescent domain of the shoot apical meristem.
See Supp Methods of Landrein et al 2018 for further details.

These scripts are part of the RegionsAnalysis package. Run the first part of the script to read the data and get the analysis done.


EXPLANATION OF THE CODE AT A GLANCE
------------------------------------

INPUT:

Folder containing just the LSM Images to analyse, see the provided Landrein_et_al dataset. To use this dataset, you will first need to unzip it. When running the different scripts, you will be asked to select the folder containing the images.

Note that some parameters might need to be set/modified to do your analysis (mostly stored in the inout structure).

OUTPUT:

Note the outcome will be dependent on the initialization parameters.

These are some of the expected outputs you should get given the default parameters.

dirdata_features.csv : CSV file where the final features are stored.

subfolders named as each input image containing
- data.mat structure, compiling different information about the image, includying its analysis.
- subfolders containing the performed analysis (e.g. intensity profile fit)
- matlab figure showing the selected ROIs, or a label in the centroid in each the ROI

dirdata.mat: matlab structure compiling different information (eg all the data.mat structures from all images)

selected_regions: folder showing the processed images with the different selected regions (i.e. the central fluorescent domain)
tiffs_orthos_max: maximal intensity orthogonal projections of the images
tiffs_with_numbered_regions

Note: this current version has improved the pipeline to find the central expression domain. Specifically, the algorithm now uses the domain that is closest to the center of mass of all the expression domains found in the image of interest (see find_central_domain function). Still, it is recommended to visually assess the outcome of the central domain.
