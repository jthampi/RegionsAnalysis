function [data]=get_basic_properties_region(inout,data,numregion)

ii=numregion;

regionmask_Pixels=data.region{numregion}.regionmask;

area_mask=sum(sum(regionmask_Pixels));

data.region{ii}.area_mask_in_pixels=area_mask;

if isfield(data,'resolution')
    if and(isfield(inout,'double_parabola'),size(data.regionlist,2)==inout.defaultnumberregions*2)  
        if and(inout.double_parabola==1,ii>inout.defaultnumberregions)
            [DX,DY]=size(data.first_image_side);  
        else
            [DX,DY]=size(data.first_image);
        end
    end

    if and(DX==data.xys_image,DY==data.xys_image)
        microns_square_per_pixel=data.microns_square_per_pixel;
    else
        microns_square_per_pixel=data.microns_square_per_pixel_orthos;
    end
    area_mask_in_microns_sq=area_mask*microns_square_per_pixel;
    data.region{ii}.area_mask_in_microns_sq=area_mask_in_microns_sq;
    data.region{ii}.area_mask_in_mm_sq=area_mask_in_microns_sq/10^6;
end


end
