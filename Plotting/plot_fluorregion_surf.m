function plot_fluorregion_surf(data,regionnumber,fluor,namestr)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;

regionmask=data.region{ii}.regionmask;
[row, col]=find(regionmask);


dirout=strcat('region_',num2str(regionnumber));

font=18;
name=strcat(namestr,'_',data.channels_string,'.pdf');

outfilename=fullfile(data.outdatapath,dirout,name);

if data.microns_per_pixel==1
    xlab='x [A.U.]';
    ylab='y [A.U.]';

else
    xlab='x [um]';
    ylab='y [um]';

end



ind=0; hwait=waitbar(0,'% of progress for plot generator'); % generates a waitbar
for k=1:data.time_frames
    waitbar(1.0*ind/(data.time_frames-1)); ind=ind+1; 
    h=figure();
    
    regionmask=double(regionmask);
    regionmask(regionmask==0)=NaN;

    
    flourinmask=double(regionmask).*double(fluor); 

    
    boundregionmask=regionmask(min(row):max(row),min(col):max(col));
    boundflourinmask=flourinmask(min(row):max(row),min(col):max(col));


    reduce=1;

    regiontoplot=double(boundflourinmask(1:reduce:end,1:reduce:end));

    figure()
    surfc(regiontoplot)
    shading interp

    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font); zlabel('Fluorescense [A.U.]','fontsize',font)
    set(gca,'fontSize',font);
    hh=gcf;
    savefig(outfilename(1:length(outfilename)-3));
    save2pdf(outfilename,hh);
end

close(hwait);




