function export_working_image_to_tiff(inout,data)
% This function is exporting the working image (note this is not a snap)

name=strcat(data.rootstring_first_image_name,'.tiff');
fullname=fullfile(inout.out_working_tiffs_path,name);

imwrite(data.selected_working_image,fullname);
       
end











