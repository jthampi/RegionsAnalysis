function [data]=intensity_in_orthos(inout,data)


for ii=1:2 % loop for the xz and yz orthos
    
    if inout.background_subtraction==1
        max_image=double(data.ortho_planes{ii}.max_backgr_sub);
        sum_image=data.ortho_planes{ii}.sum_backgr_sub;
        %error('Background subtraction in orthos is not available yet');
    else
        max_image=double(data.ortho_planes{ii}.max);  
        sum_image=data.ortho_planes{ii}.sum;
    end

    regionmask_Pixels=data.ortho_max_domain_mask{ii};
    area_mask=sum(sum(regionmask_Pixels));

    if isfield(data,'resolution')
        [DX,DY]=size(max_image);
        if and(DX==data.xys_image,DY==data.xys_image)
            microns_cube_per_pixel=data.microns_square_per_pixel*data.microns_per_pixel_orthos;
        else
            microns_cube_per_pixel=data.microns_square_per_pixel_orthos*data.microns_per_pixel;
        end
    end

    data.ortho_planes{ii}.ortho_max_intensity=sum(sum(regionmask_Pixels.*max_image));
    data.ortho_planes{ii}.ortho_sum_intensity=sum(sum(regionmask_Pixels.*sum_image));

    % zoom correction if the field exists 
    if isfield(data,'resolution')
        data.ortho_planes{ii}.ortho_max_intensity=microns_cube_per_pixel*data.ortho_planes{ii}.ortho_max_intensity;
        data.ortho_planes{ii}.ortho_sum_intensity=microns_cube_per_pixel*data.ortho_planes{ii}.ortho_sum_intensity;
    end

end % end of the xz and yz loop

end


