function [data]=intensity_in_region(inout,data,regionnumber)

% Intensity_in_region
% Note it is different for lsm files and for tiff files

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;

sidepic=0;
if and(isfield(inout,'double_parabola'),size(data.regionlist,2)==inout.defaultnumberregions*2)  
    if inout.double_parabola==1  
        sidepic=1;        
    end
end

if and(sidepic==1,ii>inout.defaultnumberregions)
    selected_image_set=zeros(data.image_side_size(1),data.image_side_size(2),2,data.time_frames);
else
    selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
end
    
if inout.background_subtraction==1
    
    if or(inout.islsm==1,inout.islif)==1 
        selected_image_set(:,:,1,1)=data.first_image_backgr_sub;
    else
        displ('please update intensity_in_region')
        return
    end
else
    
    if or(inout.islsm==1,inout.islif==1) 
        if and(sidepic==1,ii>inout.defaultnumberregions)
            selected_image_set(:,:,1,1)=data.first_image_side;
        else
            selected_image_set(:,:,1,1)=data.first_image;
        end
    else
        if inout.timecourse==1
            % Note I might want to extend it also for signal quantification
            if inout.plot_timecourse_signal==1
                selected_image_set(:,:,1,:)=data.fluor_signal_timecourse;
            else
                selected_image_set(:,:,1,:)=data.fluor_marker_timecourse;
            end
        else
            selected_image_set(:,:,1,1)=data.first_image;
        end
    end
end

regionmask_Pixels=data.region{ii}.regionmask;
area_mask=sum(sum(regionmask_Pixels));

data.region{ii}.area_mask_in_pixels=area_mask;
if isfield(data,'resolution')
    [DX,DY]=size(selected_image_set(:,:,1,1));
    if and(DX==data.xys_image,DY==data.xys_image)
        microns_square_per_pixel=data.microns_square_per_pixel;
        if inout.work_on_orthos==1
            microns_cube_per_pixel=data.microns_square_per_pixel*data.microns_per_pixel_orthos;
        end
    else
        microns_square_per_pixel=data.microns_square_per_pixel_orthos;
        if inout.work_on_orthos==1
            microns_cube_per_pixel=data.microns_square_per_pixel_orthos*data.microns_per_pixel;
        end
    end
    area_mask_in_microns_sq=area_mask*microns_square_per_pixel;
    data.region{ii}.area_mask_in_microns_sq=area_mask_in_microns_sq;
end

if data.time_frames>1
hwait=waitbar(0,'% of progress for time course profile computation'); % generates a waitbar
end

for k=1:data.time_frames
    data.region{ii}.intensity(k)=sum(sum(regionmask_Pixels.*selected_image_set(:,:,1,k)));
    % zoom correction if the field exists 
    if isfield(data,'resolution')        
        if inout.work_on_orthos==1 % PFJ REVISE! it should rather be if 3D vs 2D. but need to check the pipeline
            data.region{ii}.intensity(k)=microns_cube_per_pixel*data.region{ii}.intensity(k);
        else
            data.region{ii}.intensity(k)=microns_square_per_pixel*data.region{ii}.intensity(k);
        end
        data.region{ii}.mean_intensity(k)=data.region{ii}.intensity(k)/area_mask_in_microns_sq;
    else
        data.region{ii}.mean_intensity(k)=data.region{ii}.intensity(k)/area_mask;
    end
end

if data.time_frames>1
    close(hwait);
end

end


