function [dirdata]=extract_features_stats(inout,dirdata)

dirdata.stats=[];
num_datasets=size(dirdata.dataset,2);

for uu=1:size(dirdata.features_for_stats,2)
    field_for_stats=dirdata.features_for_stats{uu};
    xx=getfield(dirdata.features,field_for_stats);
    if size(xx,2)==1 
        average=mean(xx);
    %elseif size(xx,2)>1  % this would not be right, cause if regions are
    %different, it would involve zeros!!
    %    average=mean(mean(xx)); 
        dirdata.stats = setfield(dirdata.stats,field_for_stats,average);
    end
end

dirdata.features.namefiles{num_datasets+1,1}='Averages';

if isfield(dirdata.stats,'exp_exp')
    dirdata.features.exp_exp(num_datasets+1,1) = dirdata.stats.exp_exp;
end

if isfield(dirdata.stats,'hill_exp')
    dirdata.features.hill_exp(num_datasets+1,1) = dirdata.stats.hill_exp;
end
    
end