function [dirdata,datapath]=import_dirdata(datapath,data_structure_filename)
% import_data function loads the data structure form the data.mat file.

if(nargin < 1)
datapath = uigetdir('/','Select the data set location');
data_structure_filename='data';
end

src=load(fullfile(datapath,['dir' data_structure_filename,'.mat']))
dirdata=src.dirdata;
end