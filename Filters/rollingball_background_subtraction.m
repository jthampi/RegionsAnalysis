function [data]=rollingball_background_subtraction(inout,data)

% Note this rolling ball background removal is just applied to the marker
% by default



ballsize=inout.ballsize;

if inout.timecourse==0
    
    if(isfield(data,'fluor_marker_timecourse'))
        image_input=data.fluor_marker_timecourse;
    else
        image_input=data.selected_working_image;
    end
    
   % if (isa(image_input,'double') & max(max(image_input))>256)
   %     im2=mat2gray(image_input,[0,2^16-1]);
    %else
    %     im2=mat2gray(image_input);       
    %end
    %I=im2uint8(im2);
    
    I=image_input;
    background = imopen(I,strel('disk',ballsize));
    out=I-background;
    figure();imshow(mat2gray(out));
    
    if(isfield(data,'fluor_marker_timecourse'))
        data.fluor_marker_timecourse=out;
    else
        data.selected_working_image=out;
    end        

else if inout.timecourse==1
        
    for j=1:size(data.fluor_marker_timecourse,4)
        image_input=data.fluor_marker_timecourse(:,:,1,j);
        %im2=mat2gray(image_input);
        %I=im2uint8(im2);
        I=image_input;
        background = imopen(I,strel('disk',ballsize));
        out(:,:,1,j)=I-background;
        figure();imshow(mat2gray(out(:,:,1,j)));
    end

if inout.rolling_ball_background_filter_in_signal==1
    for j=1:size(data.fluor_signal_timecourse,4)
        image_input=data.fluor_signal_timecourse(:,:,1,j);
        %im2=mat2gray(image_input);
        %I=im2uint8(im2);
        I=image_input;
        background = imopen(I,strel('disk',ballsize));
        outs(:,:,1,j)=I-background;
        figure();imshow(mat2gray(outs(:,:,1,j)));
    end
end

    close all
    data.fluor_marker_timecourse=out;
    
if inout.rolling_ball_background_filter_in_signal==1
    data.fluor_signal_timecourse=outs;
end
end

end

% Display the Background Approximation as a Surface

% figure;
%surf(double(background(1:20:end,1:20:end))),zlim([0 255]);
% surf(double(background(1:20:end,1:20:end)));
% ax = gca;
% ax.YDir = 'reverse';