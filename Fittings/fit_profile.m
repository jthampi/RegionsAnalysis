
function [rsquare,beta]=fit_profile(func,par0,xpoints,ypoints)
opts = statset('MaxIter',2000,'FunValCheck','off');
[beta,R]=nlinfit(xpoints,ypoints,func,par0,opts);
[rsquare]=compute_rsquare(R,ypoints);
end