function [data]=make_kymo_region(inout,data,regionnumber)

% make_kymo_region computes and plots the intensity kymographs along the x and y
% direction of the rectangle in its center.

ii=regionnumber;

selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);

if inout.background_subtraction==1
    %selected_image_set=data.back_YFP_sub_fluor_timecourse;     
    selected_image_set(:,:,1,1)=data.first_image_backgr_sub;   
else
    %selected_image_set=zeros(data.image_size(1),data.image_size(2),2,data.time_frames);
    %selected_image_set(:,:,1,data.time_frames)=data.first_image;    
    if inout.islsm==0
        selected_image_set=data.fluor_marker_timecourse;
    elseif inout.islsm==1
        selected_image_set(:,:,1,data.time_frames)=data.first_image;
    end
end

microns_per_pixel=data.microns_per_pixel;
rect_length=(data.region{regionnumber}.pos1(1)-data.region{regionnumber}.pos0(1))*microns_per_pixel;

regionmask_Pixels=data.region{regionnumber}.regionmask;

ind=0; hwait=waitbar(0,'% of progress for time course profile computation'); % generates a waitbar

[row, col]=find(regionmask_Pixels);

for k=1:data.time_frames

    waitbar(1.0*ind/data.time_frames); ind=ind+1; 
    
    image=selected_image_set(:,:,1,k);
    image_tomeasure=double(regionmask_Pixels).*double(image);
    
    % extracting the portion of the image to measure
    bound_image_tomeasure=image_tomeasure(min(row):max(row),min(col):max(col));
    
    kymoy(:,k)=bound_image_tomeasure(floor(size(bound_image_tomeasure,1)/2),:); 
    kymox(:,k)=bound_image_tomeasure(:,floor(size(bound_image_tomeasure,2)/2));            

end

kymoy=kymoy';
kymox=kymox';

data.region{ii}.kymoy=kymoy; 
data.region{ii}.kymox=kymox; 

h=figure();
GP=subplot(1,2,1),imshow(mat2gray(kymoy),'Colormap',morgenstemning);
GP=subplot(1,2,2),imshow(mat2gray(kymox),'Colormap',morgenstemning);

dirout=strcat('region_',num2str(regionnumber));

name=strcat('kymo_','region',num2str(regionnumber),'_',data.setected_channel_string,'.jpeg');

outfilename=fullfile(data.outdatapath,dirout,name);

saveas(h,outfilename)

close(hwait);


h=figure();
imshow(mat2gray(kymox))

dirout=strcat('region_',num2str(regionnumber));

name=strcat('gkymox_','region',num2str(regionnumber),'_',data.setected_channel_string,'.tiff');

outfilename=fullfile(data.outdatapath,dirout,name);
saveas(h,outfilename);



h=figure();
imshow(mat2gray(kymoy))

dirout=strcat('region_',num2str(regionnumber));

name=strcat('gkymoy_','region',num2str(regionnumber),'_',data.setected_channel_string,'.tiff');

outfilename=fullfile(data.outdatapath,dirout,name);
saveas(h,outfilename)



end




