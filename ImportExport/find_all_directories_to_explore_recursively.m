function [dir_to_explore,main_directory]=find_all_directories_to_explore_recursively(varargin)

% This function finds all the subdirectories within a main_directory in a
% recursive manner, up to the 6th level, containing lsm files (and the
% main_directory is counted if it contains lsm files too).

% Just applicable to lsm file analysis pipelines.

if isempty(varargin)
    str = which(check_computer);
    [inout.codefolder,inout.datafolder]=check_computer;
    datafolder=inout.datafolder;
    main_directory= uigetdir(datafolder,'Select the data set location');
%else
%    varargin{1}
end

if ismac
    [RD]=rdir(fullfile(main_directory,'/**/**/**/**/*'));
else
    [RD]=rdir(fullfile(main_directory,'\**\**\**\**\**\**\*'));
end

dir_to_explore=[];
j=0;

% Checking if main directory contains lsm or tif files
Dlsm = dir(fullfile(main_directory,'*.lsm'));
Dtif = dir(fullfile(main_directory,'*.tif'));
Dlif = dir(fullfile(main_directory,'*.lif'));

if size(Dlsm,1)>0
    j=j+1;
    dir_to_explore{j}=main_directory;
end

if size(Dlif,1)>0
    j=j+1;
    dir_to_explore{j}=main_directory;
end

if isfield(inout,'data_files_format')
    if inout.data_files_format=='tif'
        if size(Dtif,1)>0
            j=j+1;
            dir_to_explore{j}=main_directory;
        end
    end  
end


% Going through the subdirectories
for k=1:size(RD,1)
    if isdir(RD(k).name)
        Dlsm = dir(fullfile(RD(k).name,'*.lsm'));
        if size(Dlsm,1)>0
            j=j+1;
            dir_to_explore{j}=RD(k).name;
        end
        
    end
end

if isfield(inout,'data_files_format')
    if inout.data_files_format=='tif'
        for k=1:size(RD,1)
            if isdir(RD(k).name)
                Dtif = dir(fullfile(RD(k).name,'*.tif'));
                if size(Dtif,1)>0
                    j=j+1;
                    dir_to_explore{j}=RD(k).name;
                end

            end
        end
    end
end


for k=1:size(RD,1)
    if isdir(RD(k).name)
        Dlif = dir(fullfile(RD(k).name,'*.lif'));
        if size(Dlif,1)>0
            j=j+1;
            dir_to_explore{j}=RD(k).name;
        end

    end
end


dir_to_explore=unique(dir_to_explore);

end