function [data]=make_parabola_profile(inout,data,regionnumber,xpoints,ypoints)
% 
font=20;

if isfield(data,'resolution')
    if and(data.resolution(2)~=data.resolution(1),inout.work_on_orthos==0)
        error('make sure resolutions are correctly assigned in make_parabole_profile function')
    end
    
    if and(isfield(inout,'double_parabola'),size(data.regionlist,2)==inout.defaultnumberregions*2)  
        if and(inout.double_parabola==1,regionnumber>inout.defaultnumberregions)
            [DX,DY]=size(data.first_image_side);  
        else
            [DX,DY]=size(data.first_image);
        end
    else
         [DX,DY]=size(data.first_image);
    end
    
    if and(DX==data.xys_image,DY==data.xys_image)
        xpoints=data.microns_per_pixel*xpoints;
        ypoints=data.microns_per_pixel*ypoints;
        resolution=data.microns_per_pixel;
    else
        xpoints=data.microns_per_pixel_orthos*xpoints;
        ypoints=data.microns_per_pixel_orthos*ypoints;
        resolution=data.microns_per_pixel_orthos;
    end        
end
Oxpoints=xpoints;
Oypoints=ypoints;

Fparabola= @(par,x) par(1)*(x-par(4)).^2+par(2)*(x-par(4))+par(3);
par0 = [-1  1 min(ypoints) (max(xpoints)+min(xpoints))/2];
par0bis = [1  1 min(ypoints) (max(xpoints)+min(xpoints))/2];
[rsquare_parabola,par_parabola]=fit_profile(Fparabola,par0,xpoints,ypoints);
if rsquare_parabola<0
    [rsquare_parabola,par_parabola]=fit_profile(Fparabola,par0bis,xpoints,ypoints);
end

%%%%%LineFit
% Fline= @(par,x) par(1)*(x-par(3))+par(2);
% par0 = [1  min(ypoints) (max(xpoints)+min(xpoints))/2];
% [rsquare_line,par_line]=fit_profile(Fline,par0,Oxpoints,Oypoints);
%%%%%

step_angle=1;
angle=0;
sign=0;
stop=0;
apex=par_parabola(4)-par_parabola(2)/(2*par_parabola(1));
iniborder=min(xpoints)+(max(xpoints)-min(xpoints))/3;
finborder=max(xpoints)-(max(xpoints)-min(xpoints))/3;

if or(abs(par_parabola(1))<0.01, or(apex<iniborder,apex>finborder)) 
    inimean=mean(ypoints(1:numel(ypoints)/3));
    midmean=mean(ypoints(numel(ypoints)/3:2*numel(ypoints)/3));
    finmean=mean(ypoints(2*numel(ypoints)/3:numel(ypoints)));
    if and(abs(finmean-inimean)<abs(finmean-midmean),abs(finmean-inimean)<abs(finmean-midmean))
        stop=1;        
    end
    while (stop==0)
        angle=angle+step_angle;
        if sign>=0
            regionmask_Pixels=imrotate(data.region{regionnumber}.regionmask,angle,'crop');
    %         figure();imshow(regionmask_Pixels)
            [new_ypoints,new_xpoints]=find(regionmask_Pixels);
            new_xpoints=new_xpoints*resolution;
            new_ypoints=new_ypoints*resolution;
            % figure();plot(pos_curve_x,pos_curve_y)
            if length(new_xpoints)-length(unique(new_xpoints))>length(new_ypoints)-length(unique(new_ypoints)) %Check proper X/Y assignation
                [new_ypoints,new_xpoints]=find(regionmask_Pixels.');
                new_xpoints=new_xpoints*resolution;
                new_ypoints=new_ypoints*resolution;     
            end
            % figure();plot(pos_curve_x,pos_curve_y)
            Mean=mean(new_ypoints);
            if and(abs(Mean)<abs(new_ypoints(end)),abs(Mean)<abs(new_ypoints(1)))    %Check parabola orientation 
                new_ypoints=-new_ypoints;
                new_xpoints=-new_xpoints;
            end
            par0 = [-1  1 min(new_ypoints) (max(new_xpoints)+min(new_xpoints))/2];
            [new_rsquare,new_par_parabola]=fit_profile(Fparabola,par0,new_xpoints,new_ypoints);
                        
            new_inimean=mean(new_ypoints(1:numel(new_ypoints)/3));
            new_finmean=mean(new_ypoints(2*numel(new_ypoints)/3:numel(new_ypoints)));
            diffnew=abs(new_finmean-new_inimean);
            apex=new_par_parabola(4)-new_par_parabola(2)/(2*new_par_parabola(1));
            iniborder=min(new_xpoints)+(max(new_xpoints)-min(new_xpoints))/3;
            finborder=max(new_xpoints)-(max(new_xpoints)-min(new_xpoints))/3;
            
            if and(abs(new_finmean-new_inimean)<=abs(finmean-inimean),or(apex<iniborder,apex>finborder))
                stop=0;
                sign=1;
                rsquare_parabola=new_rsquare;
                par_parabola=new_par_parabola;
                xpoints=new_xpoints;
                ypoints=new_ypoints;
                inimean=new_inimean;
                finmean=new_finmean;
            else
                stop=1;
            end
        end
        if sign<=0
            regionmask_Pixels=imrotate(data.region{regionnumber}.regionmask,-angle,'crop');
    %         figure();imshow(regionmask_Pixels)
            [new_ypoints,new_xpoints]=find(regionmask_Pixels);
            new_xpoints=new_xpoints*resolution;
            new_ypoints=new_ypoints*resolution;
            % figure();plot(pos_curve_x,pos_curve_y)
            if length(new_xpoints)-length(unique(new_xpoints))>length(new_ypoints)-length(unique(new_ypoints)) %Check proper X/Y assignation
                [new_ypoints,new_xpoints]=find(regionmask_Pixels.');    
                new_xpoints=new_xpoints*resolution;
                new_ypoints=new_ypoints*resolution; 
            end
            % figure();plot(pos_curve_x,pos_curve_y)
            Mean=mean(new_ypoints);
            if and(abs(Mean)<abs(new_ypoints(end)),abs(Mean)<abs(new_ypoints(1)))    %Check parabola orientation 
                new_ypoints=-new_ypoints;
                new_xpoints=-new_xpoints;
            end
            par0 = [-1  1 min(new_ypoints) (max(new_xpoints)+min(new_xpoints))/2];
            [new_rsquare,new_par_parabola]=fit_profile(Fparabola,par0,new_xpoints,new_ypoints); 
                        
            new_inimean=mean(new_ypoints(1:numel(new_ypoints)/3));
            new_finmean=mean(new_ypoints(2*numel(new_ypoints)/3:numel(new_ypoints)));
            apex=new_par_parabola(4)-new_par_parabola(2)/(2*new_par_parabola(1));
            iniborder=min(new_xpoints)+(max(new_xpoints)-min(new_xpoints))/3;
            finborder=max(new_xpoints)-(max(new_xpoints)-min(new_xpoints))/3;
            
            if or(abs(new_finmean-new_inimean)<=abs(finmean-inimean),or(apex<iniborder,apex>finborder))
                stop=0;
                sign=-1;
                rsquare_parabola=new_rsquare;
                par_parabola=new_par_parabola;
                xpoints=new_xpoints;
                ypoints=new_ypoints;
                inimean=new_inimean;
                finmean=new_finmean;
            else
                stop=1;
            end
        end  
    end
end

step_angle=1;
stop=0;
% figure();imshow(data.region{regionnumber}.regionmask)
while and(abs(par_parabola(1))>0.01,and(rsquare_parabola<0.99,stop==0))
    angle=angle+step_angle;
    if sign>=0
        regionmask_Pixels=imrotate(data.region{regionnumber}.regionmask,angle,'crop');
%         figure();imshow(regionmask_Pixels)
        [new_ypoints,new_xpoints]=find(regionmask_Pixels);
        new_xpoints=new_xpoints*resolution;
        new_ypoints=new_ypoints*resolution;
        % figure();plot(pos_curve_x,pos_curve_y)
        if length(new_xpoints)-length(unique(new_xpoints))>length(new_ypoints)-length(unique(new_ypoints)) %Check proper X/Y assignation
            [new_ypoints,new_xpoints]=find(regionmask_Pixels.');   
            new_xpoints=new_xpoints*resolution;
            new_ypoints=new_ypoints*resolution; 
        end
        % figure();plot(pos_curve_x,pos_curve_y)
        Mean=mean(new_ypoints);
        if and(abs(Mean)<abs(new_ypoints(end)),abs(Mean)<abs(new_ypoints(1)))    %Check parabola orientation 
            new_ypoints=-new_ypoints;
            new_xpoints=-new_xpoints;
        end
        par0 = [-1  1 min(new_ypoints) (max(new_xpoints)+min(new_xpoints))/2];
        [new_rsquare,new_par_parabola]=fit_profile(Fparabola,par0,new_xpoints,new_ypoints);

        if new_rsquare>rsquare_parabola
            stop=0;
            sign=1;
            rsquare_parabola=new_rsquare;
            par_parabola=new_par_parabola;
            xpoints=new_xpoints;
            ypoints=new_ypoints;
        else
            stop=1;
        end
    end
    if sign<=0
        regionmask_Pixels=imrotate(data.region{regionnumber}.regionmask,-angle,'crop');
%         figure();imshow(regionmask_Pixels)
        [new_ypoints,new_xpoints]=find(regionmask_Pixels);
        new_xpoints=new_xpoints*resolution;
        new_ypoints=new_ypoints*resolution;
        % figure();plot(pos_curve_x,pos_curve_y)
        if length(new_xpoints)-length(unique(new_xpoints))>length(new_ypoints)-length(unique(new_ypoints)) %Check proper X/Y assignation
            [new_ypoints,new_xpoints]=find(regionmask_Pixels.');    
            new_xpoints=new_xpoints*resolution;
            new_ypoints=new_ypoints*resolution; 
        end
        % figure();plot(pos_curve_x,pos_curve_y)
        Mean=mean(new_ypoints);
        if and(abs(Mean)<abs(new_ypoints(end)),abs(Mean)<abs(new_ypoints(1)))    %Check parabola orientation 
            new_ypoints=-new_ypoints;
            new_xpoints=-new_xpoints;
        end
        par0 = [-1  1 min(new_ypoints) (max(new_xpoints)+min(new_xpoints))/2];
        [new_rsquare,new_par_parabola]=fit_profile(Fparabola,par0,new_xpoints,new_ypoints);

        if new_rsquare>rsquare_parabola
            stop=0;
            sign=-1;
            rsquare_parabola=new_rsquare;
            par_parabola=new_par_parabola;
            xpoints=new_xpoints;
            ypoints=new_ypoints;
        else
            stop=1;
        end     
    end
end

angle=angle*sign;
if stop==1
    if sign>=0
        angle=angle-step_angle;
    else
        angle=angle+step_angle;
    end
end

%Horizontal parabolas no longer needed:
% [rsquare_parabola_inv,par_parabola_inv]=fit_profile(Fparabola,par0,ypoints,xpoints);
% if rsquare_parabola_inv<0
%     [rsquare_parabola_inv,par_parabola_inv]=fit_profile(Fparabola,par0bis,ypoints,xpoints);
% end
% 
% if abs(rsquare_parabola_inv)>abs(rsquare_parabola)
%   data.region{regionnumber}.fitparabola_type='Horizontal';
%   par_parabola=par_parabola_inv;
%   [sypoints,idx] = sort(ypoints); % sort just the first column
%   sxpoints= xpoints(idx); 
%   % transposing stuff
%   xpoints=sypoints;
%   ypoints=sxpoints;
% else
%   data.region{regionnumber}.fitparabola_type='Vertical';
% end
% 
% may=max(ypoints);
% miy=min(ypoints);

% Defining the two fitted functions
Fparabolafit= @(x) par_parabola(1)*(x-par_parabola(4)).^2+par_parabola(2)*(x-par_parabola(4))+par_parabola(3);


%%%%%Line Fit
%Flinefit= @(x) par_line(1)*(x-par_line(3))+par_line(2);
% if rsquare_line>rsquare_parabola
%     data.region{regionnumber}.fitparabola_pars=par_line;
%     data.region{regionnumber}.fitparabola_rsquare=rsquare_line;
%     data.region{regionnumber}.fitparabola_height0=0;
%     data.region{regionnumber}.fitparabola_dheight=0;
%     data.region{regionnumber}.fitparabola_radius0=(xpoints(end)-xpoints(1))/2;
%     data.region{regionnumber}.fitparabola_radius1=(xpoints(end)-xpoints(1))/2;
%     data.region{regionnumber}.fitparabola_index0=0;
%     data.region{regionnumber}.fitparabola_index1=0;
% else
%     data.region{regionnumber}.fitparabola_pars=par_parabola;
%     data.region{regionnumber}.fitparabola_rsquare=rsquare_parabola;
%     
%     % finding the apex of the parabola
%     xmax=par_parabola(4)-par_parabola(2)/(2*par_parabola(1)); 
%     ymax=Fparabolafit(xmax);
% 
%     [x0,inx0]=min(xpoints);
%     [x1,inx1]=max(xpoints);
%     y0=Fparabolafit(x0);
%     y1=Fparabolafit(max(xpoints));
% 
%     if par_parabola(1)<0
%         [ymax_man,indy]=max(ypoints);
%         xmax_man=xpoints(indy);
%     else
%         [ymax_man,indy]=min(ypoints);
%         xmax_man=xpoints(indy);
%     end
%     % Defining the first and second primordia
%     if par_parabola(1)<0
%         if y0>y1
%             yp0=y0;xp0=x0;
%             yp1=y1;xp1=x1;
%             yp0_man=ypoints(inx0);yp1_man=ypoints(inx1);       
%         else
%             yp0=y1;xp0=x1;
%             yp1=y0;xp1=x0; 
%             yp0_man=ypoints(inx1);yp1_man=ypoints(inx0);       
%         end
%     else
%         if y0<y1
%             yp0=y0;xp0=x0;
%             yp1=y1;xp1=x1;
%             yp0_man=ypoints(inx0);yp1_man=ypoints(inx1);       
%         else
%             yp0=y1;xp0=x1;
%             yp1=y0;xp1=x0;
%             yp0_man=ypoints(inx1);yp1_man=ypoints(inx0);       
%         end
%     end
%     
%     height0=abs(ymax-yp0);
%     radius0=abs(xmax-xp0);
%     index0=height0/radius0;
%     height1=abs(ymax-yp1);
%     radius1=abs(xmax-xp1);
%     index1=height1/radius1;
%     dheight=abs(yp0-yp1);
%     
%     data.region{regionnumber}.fitparabola_height0=height0;
%     data.region{regionnumber}.fitparabola_dheight=dheight;
%     data.region{regionnumber}.fitparabola_radius0=radius0;
%     data.region{regionnumber}.fitparabola_radius1=radius1;
%     data.region{regionnumber}.fitparabola_index0=index0;
%     data.region{regionnumber}.fitparabola_index1=index1;
% end
%%%%%%

%%%%%No Linear Fit
data.region{regionnumber}.fitparabola_pars=par_parabola;
data.region{regionnumber}.fitparabola_rsquare=rsquare_parabola;

% finding the apex of the parabola
xmax=par_parabola(4)-par_parabola(2)/(2*par_parabola(1)); 
ymax=Fparabolafit(xmax);

[x0,inx0]=min(xpoints);
[x1,inx1]=max(xpoints);
y0=Fparabolafit(x0);
y1=Fparabolafit(max(xpoints));

if par_parabola(1)<0
    [ymax_man,indy]=max(ypoints);
    xmax_man=xpoints(indy);
else
    [ymax_man,indy]=min(ypoints);
    xmax_man=xpoints(indy);
end
% Defining the first and second primordia
if par_parabola(1)<0
    if y0>y1
        yp0=y0;xp0=x0;
        yp1=y1;xp1=x1;
        yp0_man=ypoints(inx0);yp1_man=ypoints(inx1);       
    else
        yp0=y1;xp0=x1;
        yp1=y0;xp1=x0; 
        yp0_man=ypoints(inx1);yp1_man=ypoints(inx0);       
    end
else
    if y0<y1
        yp0=y0;xp0=x0;
        yp1=y1;xp1=x1;
        yp0_man=ypoints(inx0);yp1_man=ypoints(inx1);       
    else
        yp0=y1;xp0=x1;
        yp1=y0;xp1=x0;
        yp0_man=ypoints(inx1);yp1_man=ypoints(inx0);       
    end
end

height0=abs(ymax-yp0);
radius0=abs(xmax-xp0);
index0=height0/radius0;
height1=abs(ymax-yp1);
radius1=abs(xmax-xp1);
index1=height1/radius1;
dheight=abs(yp0-yp1);

data.region{regionnumber}.fitparabola_curvature=abs(par_parabola(1));
data.region{regionnumber}.fitparabola_height0=height0;
data.region{regionnumber}.fitparabola_dheight=dheight;
data.region{regionnumber}.fitparabola_radius0=radius0;
data.region{regionnumber}.fitparabola_radius1=radius1;
data.region{regionnumber}.fitparabola_index0=index0;
data.region{regionnumber}.fitparabola_index1=index1;

radius0_man=abs(xmax_man-xp0);
height0_man=abs(ymax_man-yp0_man);
index0_man=height0_man/radius0_man;
height1_man=abs(ymax_man-yp1_man);
radius1_man=abs(xmax_man-xp1);
index1_man=height1_man/radius1_man;
dheight_man=abs(yp0_man-yp1_man);

data.region{regionnumber}.fitparabola_tiltangle=angle;
data.region{regionnumber}.fitparabola_height0_man=height0_man;
data.region{regionnumber}.fitparabola_dheight_man=dheight_man;
data.region{regionnumber}.fitparabola_index0_man=index0_man;
data.region{regionnumber}.fitparabola_index1_man=index1_man;

if isfield(data,'resolution')
    xlab='X [\mum]';
    ylab='Y [\mum]';
else
    xlab='X [A.U.]';
    ylab='Y [A.U.]';
end


plotfit=1;
if plotfit==1    
    h=figure();
    %%%Fit Line
%     if rsquare_line>rsquare_parabola
%         plot(xpoints,Flinefit(xpoints),'-g','linewidth',2);   % plot fit 
%     else
%         p1=plot(xpoints,Fparabolafit(xpoints),'-r','linewidth',2);   % plot fit 
%     end
    %%%
    p1=plot(xpoints,Fparabolafit(xpoints),'-r','linewidth',2);   % plot fit 
    hold on    
    p2=plot(xpoints,ypoints,'-b','linewidth',1);   % plot concentration levels 
    hold on
    p3=plot(xmax,ymax,'ko','MarkerSize',10,'MarkerFaceColor','k');
    hold on
    p4=plot(xp0,yp0,'k^','MarkerSize',10,'MarkerFaceColor','k');
    hold on
    p5=plot(xp1,yp1,'kv','MarkerSize',10,'MarkerFaceColor','k');
    hold on
    p6=plot(xp0,yp0_man,'k^','MarkerSize',10,'MarkerFaceColor','w');
    hold on
    p7=plot(xp1,yp1_man,'kv','MarkerSize',10,'MarkerFaceColor','w');
    hold on
    p8=plot(xmax_man,ymax_man,'ko','MarkerSize',10,'MarkerFaceColor','w');
   
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    %%%Fit Line
%     if rsquare_line>rsquare_parabola
%         lgd=legend(' Linear Fit','Data');
%     else
%         lgd=legend(' Parabola Fit','Data'); 
%     end
    %%%%
    lgd=legend(' Parabola Fit','Data'); 
    lgd.Location='southeast';
    xlim([min(xpoints)-10,max(xpoints)+10])
    ylim([min(ypoints)-20,max(ypoints)+10])
    
    name=strcat(data.rootstring_first_image_name,'_parabolafit_','region_',num2str(regionnumber),'.pdf');
    outfilename=fullfile(data.fittings_path,name);
    save2pdf(outfilename,h);
        
    if angle~=0        
        midX=DX/2*resolution;
        midY=DY/2*resolution;
        rotate(p1,[midX midY],angle);
        rotate(p2,[midX midY],angle);  
        rotate(p3,[midX midY],angle);
        rotate(p4,[midX midY],angle);   
        rotate(p5,[midX midY],angle);  
        rotate(p6,[midX midY],angle); 
        rotate(p7,[midX midY],angle);
        rotate(p8,[midX midY],angle);
        p1.XData=p1.XData+Oxpoints(1)-p2.XData(1);
        p1.YData=p1.YData+Oypoints(1)-p2.YData(1);
        p3.XData=p3.XData+Oxpoints(1)-p2.XData(1);
        p3.YData=p3.YData+Oypoints(1)-p2.YData(1);
        p4.XData=p4.XData+Oxpoints(1)-p2.XData(1);
        p4.YData=p4.YData+Oypoints(1)-p2.YData(1);
        p5.XData=p5.XData+Oxpoints(1)-p2.XData(1);
        p5.YData=p5.YData+Oypoints(1)-p2.YData(1);
        p6.XData=p6.XData+Oxpoints(1)-p2.XData(1);
        p6.YData=p6.YData+Oypoints(1)-p2.YData(1);
        p7.XData=p7.XData+Oxpoints(1)-p2.XData(1);
        p7.YData=p7.YData+Oypoints(1)-p2.YData(1);
        p8.XData=p8.XData+Oxpoints(1)-p2.XData(1);
        p8.YData=p8.YData+Oypoints(1)-p2.YData(1);
        p2.XData=p2.XData+Oxpoints(1)-p2.XData(1);
        p2.YData=p2.YData+Oypoints(1)-p2.YData(1);
        
        h=figure();
%         plot(Oxpoints,Flinefit(Oxpoints),'-g','linewidth',2);   % plot fit
%         hold on
        plot(p1.XData,p1.YData,'-r','linewidth',2);   % plot fit 
        hold on
        plot(p2.XData,p2.YData,'-b','linewidth',1);   % plot concentration levels 
        hold on
%         plot(Oxpoints,Oypoints,'-g','linewidth',2);   % plot fit 
%         hold on
        plot(p3.XData,p3.YData,'ko','MarkerSize',10,'MarkerFaceColor','k')
        hold on
        plot(p4.XData,p4.YData,'k^','MarkerSize',10,'MarkerFaceColor','k')
        hold on
        plot(p5.XData,p5.YData,'kv','MarkerSize',10,'MarkerFaceColor','k')
        hold on
        plot(p6.XData,p6.YData,'k^','MarkerSize',10,'MarkerFaceColor','w')
        hold on
        plot(p7.XData,p7.YData,'kv','MarkerSize',10,'MarkerFaceColor','w')
        hold on
        plot(p8.XData,p8.YData,'ko','MarkerSize',10,'MarkerFaceColor','w')

        xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
        set(gca,'fontSize',font);
        lgd=legend(' Parabola Fit','Data');
        lgd.Location='southeast';
        xlim([min(p2.XData)-10,max(p2.XData)+10])
        ylim([min(p2.YData)-20,max(p2.YData)+10])
        
        name=strcat(data.rootstring_first_image_name,'_tiltparabolafit_original_','region_',num2str(regionnumber),'.pdf');
        outfilename=fullfile(data.fittings_path,name);
        save2pdf(outfilename,h);
    end
    close(h)
end
end
