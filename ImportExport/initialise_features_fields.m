function [dirdata]=initialise_features_fields(inout,dirdata);

dirdata.features.namefiles=[];

if inout.select_region==1
    dirdata.features.region_number=[];
    dirdata.features.selected_channel_string=[];
    dirdata.features.centroid_x=[];
    dirdata.features.centroid_y=[];
end

switch inout.analysis_type
    case 'Standard'
        dirdata.features.intensity=[];
        dirdata.features.mean_intensity=[];
        dirdata.features.resolution=[];
        dirdata.features.area_in_um2=[];
    case 'Multi-z'
        dirdata.features.mean_intensity_zs_sandwitch=[];
end

if inout.make_fittings==1 
    dirdata.features.intensity_wo_basal_hill=[];
    dirdata.features.intensity_wo_basal_exp=[];
    dirdata.features.exp_max=[];
    dirdata.features.hill_max=[];
    dirdata.features.exp_exp=[];
    dirdata.features.hill_exp=[];
    dirdata.features.exp_charlength=[];
    dirdata.features.hill_charlength=[];
    dirdata.features.exp_basalintensity=[];
    dirdata.features.hill_basalintensity=[];
    dirdata.features.hill_rsquare=[];
    dirdata.features.exp_rsquare=[];
    dirdata.features.hill_rsquare=[];
    dirdata.features.exp_rsquare=[];
else
    if isfield(inout,'histomaxima')
        if inout.histomaxima==1
            dirdata.features.histomaxima=zeros(1,inout.allowed_peaks); % note this is also a kind of initialisation
        end
    end
end

if inout.get_centroids_from_found_domains==1
    dirdata.features.distcenter_in_um=[];
end

if inout.fit_fixed_exponent==1
    dirdata.features.exp_charlength_fixed_exponent=[];
    dirdata.features.hill_charlength_fixed_exponent=[];
    dirdata.features.exp_basalintensity_fixed_exponent=[];
    dirdata.features.hill_basalintensity_fixed_exponent=[];
    dirdata.features.hill_rsquare_fixed_exponent=[];
    dirdata.features.exp_rsquare_fixed_exponent=[];
    dirdata.features.intensity_wo_basal_hill_fixed_exponent=[];
    dirdata.features.intensity_wo_basal_exp_fixed_exponent=[];
end

if inout.select_background_regions==1
    dirdata.features.backgroundlevel=[];
end

%if or(inout.islsm==1,inout.islif==1)

if inout.find_orientation==1
 dirdata.features.Orientation=[];
end

if inout.find_semiaxis==1
 dirdata.features.major_axis_in_um=[];
 dirdata.features.minor_axis_in_um=[];
 dirdata.features.aspect_ratio=[];
end
%end

if inout.ask_regionclass==1
 dirdata.features.Class=[];
end

%if inout.fit_hole==1;
%    dirdata.features.exp_2charlength=[];
%    dirdata.features.hill_2charlength=[];
%end

if or(inout.find_background_through_histogram_peak==1,inout.find_timecourse_background_through_histogram_peak==1)
    dirdata.features.signal_background=[];
end

end
