function  [signal_channel,marker_channel,otherchannel,data]=get_channels_info(inout,data,chosenchannel) 
% Function executed for tiff files when inout.standard_format_filename=1
% This helps to get the strings  of the different channels

switch inout.scope
        case 'readnb'
            fullndfilename=fullfile(inout.datapath,inout.ndfilename)

            text=fileread(fullndfilename);
            TextAsCells=regexp(text, '\n', 'split');
            mask1=~cellfun(@isempty, strfind(TextAsCells, 'WaveName1'));
            string1= TextAsCells(mask1);
            channel1=string1{1}(15:end-2);

            mask2=~cellfun(@isempty, strfind(TextAsCells, 'WaveName2'));
            string2= TextAsCells(mask2);
            if not(isempty(string2))
                channel2=string2{1}(15:end-2);
                otherchannel=['2' channel2];
            else
                otherchannel=['1' channel1];
            end

            data.selected_channel_string=['1' channel1];

            if inout.ndsignal==1
                signal_channel=1;
                marker_channel=2;
            elseif inout.ndsignal==2
                %data.selected_channel_string=['2' channel2];
                signal_channel=2;
                marker_channel=1;
                %otherchannel=['1' channel1];
            end
        
        case 'spi'        
            flag=mean(chosenchannel(1:19)=='1GFP-dsRED dual GFP');
            if flag
                otherchannel='2GFP-dsRED dual DSRED';
                marker_channel=2; 
                signal_channel=1;
 %                marker_channel=1; 
 %               signal_channel=2;
                data.selected_channel_string='1GFP-dsRED dual GFP';
            else
                otherchannel='1GFP-dsRED dual GFP';
                marker_channel=1; 
               signal_channel=2;
 %                 marker_channel=2; 
 %               signal_channel=1;
                data.selected_channel_string='2GFP-dsRED dual DSRED';
            end

            flag=mean(chosenchannel(1:19)=='2GFP-dsRED dual GFP');
            flag2=mean(chosenchannel(1:19)=='1GFP-dsRED dual DSR');
            % in case DsRedChannel is first
            if flag==1
                otherchannel='1GFP-dsRED dual DSRED';
                %marker_channel=1; 
                %signal_channel=2;
                               % change for the division movies
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string='2GFP-dsRED dual GFP';
            elseif flag2==1
                otherchannel='2GFP-dsRED dual GFP';
               % marker_channel=2; 
               % signal_channel=1;
               % change for the division movies
                marker_channel=1; 
                signal_channel=2;   
                data.selected_channel_string='1GFP-dsRED dual DSRED';
            end

        case 'spu'
    
            chu='1GFP Confocal - Benoit_s';
            if chosenchannel(1:15)==chu(1:15)
                otherchannel='2DSRED Confocal';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string='1GFP Confocal - Benoit';
            else
                otherchannel='1GFP Confocal - Benoit';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='2DSRED Confocal';
            end
        case 'spo'
            chu='1YFP Confocal_s';
            if chosenchannel(1:5)==chu(1:5)
                otherchannel='2FM464 Confocal';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string='1YFP Confocal';
            else
                otherchannel='2FM464 Confocal';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='2FM464 Confocal';
            end
         case 'spy'
            chu='1DSRED Confocal_s';
            if chosenchannel(1:5)==chu(1:5)
                otherchannel='2YFP Confocal Pau';
                otherchannel='2YFP Confocal';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string='1DSRED Confocal';
            else
                otherchannel='2YFP Confocal Pau';
                otherchannel='2YFP Confocal';

                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='1DSRED Confocal';
            end           
             
            
        case 'cyano'
       
            aa=sum(findstr(chosenchannel, 'GFP'));

            if aa>0
                otherchannel='2TRICT';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string=chosenchannel;
            else
                otherchannel='3GFP';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string=chosenchannel;
            end
        case 'cy2'            
            chu='3TRITC_s';
            if chosenchannel(1:4)==chu(1:4)
                otherchannel='2GFP - Camera';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='3TRITC';
            else
                otherchannel='3TRITC';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string='2GFP - Camera';
            end
            
        case 'bacillus1'
            chu='3GFP - Camera';
            aa=sum(findstr(chosenchannel, 'GFP'));

            if aa>0
                otherchannel='2Ca Crimson - Camera';
                marker_channel=2; 
                signal_channel=1;
                data.selected_channel_string=chosenchannel;
            else
                otherchannel='3GFP - Camera';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string=chosenchannel;
            end
            
        case 'spiscan'   % if the marker is in channel 1 and signal in channel 2  
            
            chu='2';
            if chosenchannel(1:1)==chu
                otherchannel='1';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='2';
            else
                otherchannel='2';
                marker_channel=1; 
                signal_channel=2;
                data.selected_channel_string='1';
            end  
            
            case 'spiscancit'       % if the marker is in channel 2 and signal in channel 1

                chu='1';
                if chosenchannel(1:1)==chu
                    otherchannel='2';
                    marker_channel=2; 
                    signal_channel=1;
                    data.selected_channel_string='1';
                else
                    otherchannel='1';
                    marker_channel=1; 
                    signal_channel=2;
                    data.selected_channel_string='2';
                end 

end
    
end