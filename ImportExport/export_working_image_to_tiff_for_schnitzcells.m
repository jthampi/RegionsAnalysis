function export_working_image_to_tiff_for_schnitzcells(inout,data)

% This function is exporting the working image (note this is not a
% snap) with the shcnitzcells name format.

%

% for marker, '-r-', while for signal, '-g-'
 

data.schnitzdatapath=fullfile(inout.datapath,'schnitz');   
name=strcat(data.rootstring_first_image_name,'.tiff');
create_dir(data.schnitzdatapath);

timepoint=1; %assuming no timepoints
stagepos=1;

switch inout.standard_format_filename
    case 3
        newname_signal = [inout.basename,'-',num2str(stagepos,'%1.2d'),'-g-',num2str(timepoint,'%1.3d'),'.tif'];
        fullname=fullfile(data.schnitzdatapath,newname_signal);
        imwrite(data.fluor_signal_timecourse,fullname);
        if inout.number_tiff_channels==2
            newname_marker = [inout.basename,'-',num2str(stagepos,'%1.2d'),'-r-',num2str(timepoint,'%1.3d'),'.tif'];
            fullname=fullfile(data.schnitzdatapath,newname_marker);
            imwrite(data.fluor_marker_timecourse,fullname);
        end

    otherwise
        if isfield(inout,'schnitzcells_channel_string')
            channel_string=inout.schnitzcells_channel_string;
        else
            channel_string='-g-';
        end
        newname_signal = [data.rootstring_first_image_name,'-',num2str(stagepos,'%1.2d'),channel_string,num2str(timepoint,'%1.3d'),'.tif'];
        
        fullname=fullfile(data.schnitzdatapath,newname_signal);
        a=data.selected_working_image;
        if isa(a,'double')
            if max(max(a))>256
            data.selected_working_image=cast(data.selected_working_image,'uint16');
            end
        end
        imwrite(data.selected_working_image,fullname);
end


end



