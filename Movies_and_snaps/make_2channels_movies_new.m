function [M1,M2]=make_2channels_movies_new(inout,data)

% make_2channels_movies_new creates two different movies of the marker and signal data channels
% The first movie is the combined snap
% The second movie is a concatenated combined snap, the marker snap and the
% signal snap.
% The input function is the data structure, but if no inputs 
% are provided the data.mat file is imported automatically after selection of
% its directory location . 

if(nargin < 1)
datapath = uigetdir('/','Select the data set location');
[data,~]=import_data(datapath);
else
  datapath=inout.datapath;
end

signal=permute(data.fluor_signal_timecourse(:,:,1,:),[1,2,4,3]);
marker=permute(data.fluor_marker_timecourse(:,:,1,:),[1,2,4,3]);
time_frames=data.time_frames;

string_name=data.twochannels_string;


mxm=double(max(max(max(marker))));

%mxs=double(max(max(max(signal))));

sigvect=reshape(signal,[1 data.image_size(1)*data.image_size(2)*time_frames]);
markvect=reshape(marker,[1 data.image_size(1)*data.image_size(2)*time_frames]);

if isfield(inout,'saturation_percentile_marker')    
    %mxm=double(prctile(max(max(marker)),inout.saturation_percentile_marker));
    saturation_percentile_marker=inout.saturation_percentile_marker;
else
    saturation_percentile_marker=50;
    %mxm=double(prctile(max(max(marker)),50));
end

if isfield(inout,'saturation_percentile_signal')
    %mxs=double(prctile(max(max(signal)),inout.saturation_percentile_signal));
     saturation_percentile_signal=inout.saturation_percentile_signal;
else
    %mxs=double(prctile(max(max(signal)),70));
    saturation_percentile_signal=70;
end

mxm=double(prctile(markvect,saturation_percentile_marker));
mxs=double(prctile(sigvect,saturation_percentile_signal));


%%% First movie

name=strcat('Combined_channels_mov_',string_name);
%outfilename=fullfile(data.outdatapath,name);
outfilename=fullfile(datapath,name);



if inout.make_movie_combinedchannels==1 
f0 = figure; 

aviobj = VideoWriter(outfilename,'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

ind=0; h=waitbar(0,'Generating the combined channels movie'); % generates a waitbar

imm=zeros(size(marker,1),size(marker,2),3);


for k=1:time_frames
    waitbar(1.0*ind/length(time_frames)); ind=ind+1; 
   % imm(:,:,1)=1.0*marker(:,:,k)/(1.0*mxm);
  %  imm(:,:,2)=1.0*marker(:,:,k)/(1.0*mxm);
   % imm(:,:,3)=1.0*signal(:,:,k)/(1.0*mxs);

    imm(:,:,1)=mat2gray(marker(:,:,k),[0 mxm]);
    imm(:,:,2)=mat2gray(signal(:,:,k),[0 mxs]);
    imm(:,:,3)=mat2gray(marker(:,:,k),[0 mxm]);

        %  before it was  imshow(imm), but for some reason, sometimes the
    %  signal was not appearing in the movie, so I normalised again with mat2gray (not
    %  sure why is necessary ...). 
    
    imshow(mat2gray(imm)); 

    M1 = getframe(f0);
    writeVideo(aviobj, M1);
end 

close(h)
close(aviobj);

end 

%
%%% Second movie
if inout.make_movie_juxtchannels==1 

name=strcat('Juxtaposed_channels_mov_backsub_',string_name);

outfilename=fullfile(datapath,name);

f1 = figure; 

aviobj = VideoWriter(outfilename,'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

ind=0; h=waitbar(0,'Generating the juxtaposed channels movie'); % generates a waitbar

for k=1:time_frames
    waitbar(1.0*ind/length(time_frames)); ind=ind+1; 
    %juxtaposed_images=[1.0*marker(:,:,k)/mxm 0.9*ones(data.image_size(1),1) 1.0*signal(:,:,k)/(mxs)];    
    %juxtaposed_images=[mat2gray(marker(:,:,k), [0 mxm]) 0.9*ones(data.image_size(1),1) mat2gray(signal(:,:,k),[0 mxs])];                                                                                               

    white=zeros(size(marker,1),size(marker,2));
    juxtaposed_image1=[mat2gray(marker(:,:,k), [0 mxm]) 0.9*ones(data.image_size(1),1) white 0.9*ones(data.image_size(1),1) mat2gray(marker(:,:,k), [0 mxm]) ];                                                                                               
    juxtaposed_image2=[mat2gray(signal(:,:,k),[0 mxs]) 0.9*ones(data.image_size(1),1) mat2gray(signal(:,:,k),[0 mxs]) 0.9*ones(data.image_size(1),1) white ];                                                                                               
    juxtaposed_image3=[mat2gray(marker(:,:,k), [0 mxm]) 0.9*ones(data.image_size(1),1) white 0.9*ones(data.image_size(1),1) mat2gray(marker(:,:,k), [0 mxm]) ];                                                                                               
    %juxtaposed_images=[juxtaposed_image1; juxtaposed_image2; juxtaposed_image3];
    juxtaposed_images(:,:,1)=juxtaposed_image1;
    juxtaposed_images(:,:,2)=juxtaposed_image2;
    juxtaposed_images(:,:,3)=juxtaposed_image3;

    imshow(mat2gray(juxtaposed_images));

    %imshow(juxtaposed_images)
    M2 = getframe(f1);
    writeVideo(aviobj, M2);
end 

close(h)

close(aviobj);

movefile(fullfile(datapath,'*.avi'),fullfile(datapath,'MOVS'))

end

%movefile(fullfile(datapath,'*.avi'),fullfile(datapath,'MOVS'))

