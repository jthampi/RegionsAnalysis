function [intensity_radii, maskedregionfluor]=angular_integration2(image,x0,y0,maximumradius,radiusstep,microns_per_pixel,frac_angularregion)
imagesize=size(image);

% This function makes the integration of the fluorescense in a circular or
% piece of cake shaped masked region. 

% We will divide the mask into donut-shaped regions. 

%steps=floor(maximumradius/radiusstep); % Notice this can give an error,
%rounding down in very extreme cases, for instance when having 6.999999,
%giving 6.

iniradius=radiusstep;
radii=iniradius:radiusstep:maximumradius;
steps=size(radii,2);
maskedregionfluor = zeros(size(image,1), size(image,2), steps);

intensity_radii=zeros(steps,1);

for i=1:steps
    radius = radii(i);
    [circlePixels]=makecircle(radius,x0,y0,imagesize);
%     imshow(circlePixels)
    
    % This if creates the different regions with its fluorescense. The first step is a
    % circular region, and the second is a donut-shaped region.
    if i==1    
        maskedregion=circlePixels;
    else
        maskedregion=circlePixels.*(~aux_circlePixels);
    end
    maskedregionfluor(:,:,i) = maskedregion.*image;
%     imshow(maskedregionfluor(:,:,i))

    area_maskedregion=sum(sum(maskedregion));
    
    intensity_maskedregionflour=sum(sum(maskedregionfluor(:,:,i)));
    
    intensity_radii(i)=intensity_maskedregionflour/area_maskedregion;
    
    aux_circlePixels = circlePixels;
end