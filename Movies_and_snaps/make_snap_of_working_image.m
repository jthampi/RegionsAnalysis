function make_snap_of_working_image(inout,data)
% This function is exporting the working image. Note will pick from 0 to
% 255 if inout.lsm_zmax==1.

font=18;

if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end

name=strcat(strng,'.tiff');

outfilename=fullfile(inout.out_working_tiffs_path,name);

namefig=strcat('allregions_snap.fig');
outfilenamefig=fullfile(data.outdatapath,namefig);

h=figure();

if inout.zmax==1
    is=mat2gray(data.selected_working_image,[0 255]);
    %is=imshow(mat2gray(data.selected_working_image,[0 255]),'Colormap',morgenstemning);
else
    is=mat2gray(data.selected_working_image);
    %is=imshow(mat2gray(data.selected_working_image),'Colormap',morgenstemning);
end

imshow(is);
hh=getframe(h);
hhh=frame2im(hh) ;
imwrite(hhh,outfilename);

%J = step(shapeInserter,I,PTS) 
%t = Tiff(outfilename,'w');
%t.write(hhh);
%t.close();


%print -dpdf -painters epsFig
end
