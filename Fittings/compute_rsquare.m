
function [rsquare]=compute_rsquare(Rvect,ypoints)
SSresidual=sum(Rvect.^2);
SStotal=sum((ypoints-mean(ypoints)).^2);
rsquare=1-(SSresidual/SStotal);
end


