
function [Dfiles_out] = find_non_analysed_files(inout,Dfiles)
% This function returns the list of files that do not have a subdirectory
% in its name - i.e., those files that for sure have not been analysed.


numfiles=length(Dfiles);
index=0;
for ifiles=1:numfiles  % Starting the directory loop
    fullname=Dfiles(ifiles).name ;
    fin_rootstring=strfind(fullname,'.');
    rootstring_fullname=fullname(1:fin_rootstring-1);
    ext=fullname(fin_rootstring+1:end);
    
    
    fulldirpath=fullfile(inout.datapath,rootstring_fullname);
    if not(mean(ext=='TIF'))
        ex=exist(fulldirpath);
        if ex==0
            index=index+1;
           % Dfiles_out(index).name=fullname;
            Dfiles_out(index)=Dfiles(ifiles);
        end
    end

    if mean(ext=='TIF')
        ini_s=strfind(rootstring_fullname,'_s');
        ini_t=strfind(rootstring_fullname,'_t');
        pos=rootstring_fullname(ini_s+2:ini_t-1);
        
        movfile=['combined_channels_mov_',inout.ndfilename(1:end-3),'_s',pos,'.avi'];
        fulldirpathmov=fullfile(inout.datapath,movfile);
        ex=exist(fulldirpathmov);
        if ex==0
            index=index+1;
           % Dfiles_out(index).name=fullname;
            Dfiles_out(index)=Dfiles(ifiles);
        end
    end
end

if ~exist('Dfiles_out')
    msg='Note you are running your pipeline in non-overwrite mode (you have parameter set to inout.overwriting_mode=0). No files found that need to be analysed.';
    %error(msg)
    warning(msg)
    Dfiles_out=[];

end

end